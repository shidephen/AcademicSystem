/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;

/**
 * @author paul
 *
 */
@MyBatisDao
public interface ExamDao extends CrudDao<Exam> {

	List<Dict> getTermList();

	List<Dict> getSubjectList();

	String getExamId(Exam exam);

	void insertExamSubject(Exam exam);

	void deleteExamSubject(Exam exam);

	int getExamScoreCountById(Exam exam);

	int getExamQuestionCountById(Exam exam);

}
