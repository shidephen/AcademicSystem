/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.BaseService;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.teacher.dao.ExamDao;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;

/**
 * @author paul
 *
 */
@Service
@Transactional(readOnly = true)
public class ExamService extends BaseService {

	@Autowired
	private ExamDao examDao;

	public Page<Exam> findExam(Page<Exam> page, Exam exam) {
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		exam.getSqlMap().put("dsf", dataScopeFilter(exam.getCurrentUser(), "o", "a"));
		// 设置分页参数
		exam.setPage(page);
		// 执行分页查询
		page.setList(examDao.findList(exam));
		return page;
	}

	public Exam getExam(String id) {
		return examDao.get(id);
	}

	public List<Dict> getTermList() {
		return examDao.getTermList();
	}

	public List<Dict> getSubjectList() {
		return examDao.getSubjectList();
	}

	public void deleteExam(Exam exam) {
		examDao.delete(exam);
	}

	public void save(Exam exam) {
		String[] idArr = exam.getSubjects().split(",");
		examDao.update(exam);
		examDao.deleteExamSubject(exam);
		for (String id : idArr) {
			exam.setSubjectId(id);
			examDao.insertExamSubject(exam);
		}
	}

	public void insert(Exam exam) {
		String[] idArr = exam.getSubjects().split(",");
		examDao.insert(exam);
		String examId = examDao.getExamId(exam);
		exam.setExamId(examId);
		for (String id : idArr) {
			exam.setSubjectId(id);
			examDao.insertExamSubject(exam);
		}
	}

	public int getExamScoreCountById(Exam exam) {
		return examDao.getExamScoreCountById(exam);
	}

	public int getExamQuestionCountById(Exam exam) {
		return examDao.getExamQuestionCountById(exam);
	}
	
}
