/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.teacher.entity.AnalysisOfAll;
import com.thinkgem.jeesite.modules.teacher.service.AnalysisOfAllSerivce;

/**
 * @author yangping
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/teacher/analysisOfAll")
public class AnalysisOfAllController extends BaseController {

	@Autowired
	private AnalysisOfAllSerivce analysisOfAllSerivce;
	/**
	 * @return 考试
	 */
	@ResponseBody
	@RequestMapping(value = "getExamList")
	public List<AnalysisOfAll> getExam(String first) {
		return this.analysisOfAllSerivce.getExamList(first);
	}
	
	/**
	 * @return 班级名称
	 */
	@ResponseBody
	@RequestMapping(value = "getClassName")
	public List<AnalysisOfAll> getClassName(String first) {
		return this.analysisOfAllSerivce.getClassName(first);
	}
	/**
	 * @return 科目
	 */
	@ResponseBody
	@RequestMapping(value = "getSubjectList")
	public List<AnalysisOfAll> getSubjectList(String termCode) {
		return this.analysisOfAllSerivce.getSubjectList(termCode);
	}
	/**
	 * @return 班级点击事件
	 */
	@ResponseBody
	@RequestMapping(value = "classClick")
	public List<AnalysisOfAll> classClick(AnalysisOfAll analysis) {
		return null;
	}
	
	@RequestMapping(value = "analysis")
	public String save(AnalysisOfAll analysis, HttpServletRequest request, Model model) {
		List<AnalysisOfAll> termList = this.analysisOfAllSerivce.getTermList();
		model.addAttribute("termList", termList);
		List<AnalysisOfAll> examList = this.analysisOfAllSerivce.getExamList("2016");
		model.addAttribute("examList", examList);
		List<AnalysisOfAll> classList = this.analysisOfAllSerivce.getClassName("2016");
		model.addAttribute("classList",classList);
		List<AnalysisOfAll> subjectList= this.analysisOfAllSerivce.getSubjectList("G21");
		model.addAttribute("subjectList", subjectList );
		analysis.setFirstYear("2016");
		analysis.setTermCode(termList.get(0).getTermCode());
		analysis.setExamId(examList.get(0).getExamId());
		analysis.setClassCode(classList.get(0).getClassCode());
		analysis.setSubjectId(subjectList.get(0).getSubjectId());
		model.addAttribute("classScore",this.analysisOfAllSerivce.getClassBySubId(analysis));
		return "modules/teacher/analysisOfAll";
	}
	
}
