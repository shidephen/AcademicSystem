/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;
import com.thinkgem.jeesite.modules.teacher.service.SubjectService;

/**
 * @author songhao
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/teacher/subject")
public class SubjectController extends BaseController {

	@Autowired
	private SubjectService subjectService;
	
	@ModelAttribute
	public Subject get(@RequestParam(required=false) String id) {
		if (StringUtils.isNotBlank(id)){
			return subjectService.getSubject(id);
		}else{
			return new Subject();
		}
	}
	
	@RequiresPermissions("teacher:subject:view")
	@RequestMapping(value = {"list", ""})
	public String list(Subject subject, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Subject> page = subjectService.findSubject(new Page<Subject>(request, response), subject);
        model.addAttribute("page", page);
		return "modules/teacher/subjectManage";
	}
	
	@RequiresPermissions("teacher:subject:view")
	@RequestMapping(value = "form")
	public String form(Subject subject, Model model) {
		model.addAttribute("subject", subject);
		return "modules/teacher/subjectForm";
	}
	
	@RequiresPermissions("teacher:subject:edit")
	@RequestMapping(value = "delete")
	public String delete(Subject subject, RedirectAttributes redirectAttributes) {
		int knowledgeCount = subjectService.getKnowledgeBySubjectId(subject);
		int scoreCount = subjectService.getScoreBySubjectId(subject);
		if ((knowledgeCount + scoreCount) == 0) {
			subjectService.deleteSubject(subject);
			addMessage(redirectAttributes, "删除科目成功");
		} else if (knowledgeCount > 0 && scoreCount == 0) {
			addMessage(redirectAttributes, "删除失败：已维护的知识点不可删除！");
		} else if (knowledgeCount == 0 && scoreCount > 0) {
			addMessage(redirectAttributes, "删除失败：已维护的成绩的科目不可删除！");
		} else {
			addMessage(redirectAttributes, "删除失败：已维护知识点和成绩的科目不可删除！");
		}
		return "redirect:" + adminPath + "/teacher/subject/list?repage";
	}
	
	@RequiresPermissions("teacher:subject:edit")
	@RequestMapping(value = "save")
	public String save(Subject subject, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		// 保存考试信息
		subjectService.insert(subject);
		addMessage(redirectAttributes, "保存成功");
		return "redirect:" + adminPath + "/teacher/subject/list?repage";
	}
	
}
