/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teacher.entity.AnalysisOfAll;

/**
 * @author paul
 *
 */
@MyBatisDao
public interface AnalysisOfAllDao extends CrudDao<AnalysisOfAll> {
	
	List<AnalysisOfAll> getClassName(String firstYear);
	
	List<AnalysisOfAll> getClassScore(AnalysisOfAll analysis);
	
	List<AnalysisOfAll> getGradeScore(AnalysisOfAll analysis);

	List<AnalysisOfAll> getExamList(String firstYear);

	List<AnalysisOfAll> getSubjectList(String termCode);

	List<AnalysisOfAll> getTermList();
	
	List<AnalysisOfAll> getClassBySubId(AnalysisOfAll analysis);
	
	List<AnalysisOfAll> getClassBySubAllScore(AnalysisOfAll analysis);
	
	List<AnalysisOfAll> getGradeBySubId(AnalysisOfAll analysis);
	
	List<AnalysisOfAll> getGradeByAllScore(AnalysisOfAll analysis);

}
