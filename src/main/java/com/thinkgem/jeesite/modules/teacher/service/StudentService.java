/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.BaseService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.teacher.dao.StudentDao;
import com.thinkgem.jeesite.modules.teacher.entity.Class;
import com.thinkgem.jeesite.modules.teacher.entity.Student;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;

/**
 * @author paul
 *
 */
@Service
@Transactional(readOnly = true)
public class StudentService extends BaseService {

	@Autowired
	private StudentDao studentDao;
	
	public Page<Student> findStudent(Page<Student> page, Student student) {
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		student.getSqlMap().put("dsf", dataScopeFilter(student.getCurrentUser(), "o", "a"));
		// 设置分页参数
		student.setPage(page);
		// 执行分页查询
		page.setList(studentDao.findList(student));
		return page;
	}

	public void deleteStudent(Student student) {
		studentDao.delete(student);
	}

	public Student getStudent(String id) {
		return studentDao.get(id);
	}

	public int save(Student student) {
		return studentDao.update(student);
	}

	public int add(Student student) {
		return studentDao.insert(student);
	}

	public List<StudentScore> getClass(String firstYear) {
		return studentDao.getClass(firstYear);
	}

	public void addUser(User user) {
		user.preInsert();
		studentDao.addUser(user);
		String roleId = studentDao.getRoleId("学生系统");
		studentDao.addUserRole(user.getId(), roleId);
	}

	public void deleteStudentUser(User user) {
		studentDao.deleteStudentUser(user);
	}

	public void deleteStudentByAccount(Student student) {
		studentDao.deleteStudentByAccount(student);
	}

	public void deleteStudentScore(Student student) {
		studentDao.deleteStudentScore(student);
	}

	public void deleteClass(String schoolCode, String firstYear) {
		studentDao.deleteClass(schoolCode, firstYear);
	}

	public void addClass(Class info) {
		studentDao.addClass(info);
	}
	
}
