/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.BaseService;
import com.thinkgem.jeesite.modules.teacher.dao.SubjectDao;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;

/**
 * @author songhao
 *
 */
@Service
@Transactional(readOnly = true)
public class SubjectService extends BaseService {

	@Autowired
	private SubjectDao subjectDao;

	public Subject getSubject(String id) {
		return subjectDao.get(id);
	}

	public Page<Subject> findSubject(Page<Subject> page, Subject subject) {
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		subject.getSqlMap().put("dsf", dataScopeFilter(subject.getCurrentUser(), "o", "a"));
		// 设置分页参数
		subject.setPage(page);
		// 执行分页查询
		page.setList(subjectDao.findList(subject));
		return page;
	}
	
	public int getKnowledgeBySubjectId(Subject subject) {
		return subjectDao.getKnowledgeBySubjectId(subject.getSubjectId());
	}
	
	public int getScoreBySubjectId(Subject subject) {
		return subjectDao.getScoreBySubjectId(subject.getSubjectId());
	}

	public void deleteSubject(Subject subject) {
		subjectDao.delete(subject);
	}

	public void insert(Subject subject) {
		subjectDao.insert(subject);
	}
	
}
