/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.teacher.dao.AnalysisOfAllDao;
import com.thinkgem.jeesite.modules.teacher.entity.AnalysisOfAll;

/**
 * @author paul
 *
 */
@Service
@Transactional(readOnly = true)
public class AnalysisOfAllSerivce extends  CrudService<AnalysisOfAllDao, AnalysisOfAll> {

	@Autowired
	private AnalysisOfAllDao analysisOfAllDao;
	
	public List<AnalysisOfAll> getClassName (String firstYear) {
		return this.analysisOfAllDao.getClassName(firstYear);
	}

	public List<AnalysisOfAll> getExamList(String first) {
		return analysisOfAllDao.getExamList(first);
	}

	public List<AnalysisOfAll> getSubjectList(String termCode) {
		return analysisOfAllDao.getSubjectList(termCode);
	}

	public List<AnalysisOfAll> getTermList() {
		return analysisOfAllDao.getTermList();
	}
	
	public List<AnalysisOfAll> getClassBySubId(AnalysisOfAll analysis){
		return this.analysisOfAllDao.getClassBySubId(analysis);
	}
	
	public List<AnalysisOfAll> getClassBySubAllScore(AnalysisOfAll analysis){
		String termCode = analysis.getTermCode().substring(0, analysis.getTermCode().length()-1);
		analysis.setTermCode(termCode);
		return this.getClassBySubAllScore(analysis);
	}
	
	public List<AnalysisOfAll> getGradeBySubId(AnalysisOfAll analysis){
		return this.getGradeBySubId(analysis);
	}
	
	public List<AnalysisOfAll> getGradeByAllScore(AnalysisOfAll analysis){
		return this.getGradeByAllScore(analysis);
	}
	
}
