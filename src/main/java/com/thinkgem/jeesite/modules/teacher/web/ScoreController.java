/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.web;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.excel.ResolveExcel;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.ans.tasks.AllInTask;
import com.thinkgem.jeesite.modules.teacher.dao.ScoreDao;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;
import com.thinkgem.jeesite.modules.teacher.entity.ScoreSub;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;
import com.thinkgem.jeesite.modules.teacher.service.ScoreService;

/**
 * @author songhao
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/teacher/score")
public class ScoreController extends BaseController {
	
	@Autowired
	private ScoreService scoreService;
	
	@Autowired
	private ScoreDao scoreDao;
	
	@RequiresPermissions("teacher:score:view")
	@RequestMapping(value = {"list", ""})
	public String list(String examId, StudentScore studentScore, HttpServletRequest request, HttpServletResponse response, Model model) {
		studentScore.setExamId(examId);
		Page<StudentScore> page = scoreService.getStudentScore(new Page<StudentScore>(request, response), studentScore);
        model.addAttribute("page", page);
		return "modules/teacher/scoreManage";
	}
	
	/**
	 * 下载导入科目成绩模板
	 * @param termCode
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:score:view")
    @RequestMapping(value = "importSubjectScore/template")
    public String importSubjectScoreTemplate(String termCode, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			scoreService.importSubjectScoreTemplate(termCode, response);
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:" + adminPath + "/teacher/score/list?repage";
    }
	
	/**
	 * 导入科目成绩信息
	 * @param schoolCode
	 * @param termCode
	 * @param examId
	 * @param file
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:score:edit")
    @RequestMapping(value = "importSubjectScore", method=RequestMethod.POST)
    public String importSubjectScoreFile(String schoolCode, String termCode, MultipartFile file, RedirectAttributes redirectAttributes) {

		//解析excel
		String filename = file.getOriginalFilename();
		String examName = filename.substring(0, filename.indexOf("."));
		String suffix = filename.substring(filename.indexOf(".")+1);
		String examId = scoreService.getExamIdByName(examName);
		try {
			if (examId == null) {
				addMessage(redirectAttributes, "导入文件名称与考试名称不符！");
			} else if (suffix.toLowerCase().equals("xls") || suffix.toLowerCase().equals("xlsx")) {
				InputStream is = file.getInputStream();
				List<Map<String, Object>> mapList = null;
				if ("xls".equals(suffix.toLowerCase())) {
					mapList = ResolveExcel.readXls(is);
				}
				if ("xlsx".equals(suffix.toLowerCase())) {
					mapList = ResolveExcel.readXlsx(is);
				}
				is.close();
				// 取得当前在用科目列表并将科目名称和科目ID转化为K,V
				List<Subject> subjectList = scoreService.getSubjectLists(schoolCode, termCode);
				Map<String, String> map = new HashMap<String, String>();
				for (Subject subject : subjectList) {
					map.put(subject.getSubjectName(), subject.getSubjectId()+"");
				}
				String studentAccount = null;
	
				ScoreSub score = new ScoreSub();
				for (Map<String, Object> rowMap : mapList){
					studentAccount = rowMap.get("学生账号").toString();
					Set<String> keySet = rowMap.keySet();
					keySet.remove("学生账号");
					keySet.remove("姓名");
					keySet.remove("rowIndex");
					for (String subjectName : keySet) {
						score.setStudentAccount(studentAccount);
						score.setExamId(examId);
						score.setSubjectId(map.get(subjectName));
						score.setScore(rowMap.get(subjectName).toString().equals("-")?"0":rowMap.get(subjectName).toString());
						scoreService.deleteSubjectScore(score);
						scoreService.insertSubjectScore(score);
					}
				}
				addMessage(redirectAttributes, "导入科目成绩信息成功！");
				
				// 执行异步任务计算成绩
				Properties prop = new Properties();
				String url = null;
				String user = null;
				String password = null;
				String driver = null;
				InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("jeesite.properties");
				prop.load(inputStream);
				driver = prop.getProperty("jdbc.driver");
				url = prop.getProperty("jdbc.url");
				user = prop.getProperty("jdbc.username");
				password = prop.getProperty("jdbc.password");
				Class.forName(driver);
				Connection connection = null;
				connection = DriverManager.getConnection(url, user, password);
				AllInTask task = new AllInTask(connection);
				CompletableFuture.supplyAsync(()->task.ApplyAll(Integer.valueOf(examId)));
			} else {
				addMessage(redirectAttributes, "失败：导入文件格式不正确！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addMessage(redirectAttributes, "导入科目成绩信息失败！");
		}
		return "redirect:" + adminPath + "/teacher/score/list?repage";
    }
	
	/**
	 * 下载导入科目试题成绩模板
	 * @param termCode
	 * @param examId
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:score:view")
    @RequestMapping(value = "importQuestionScore/template")
    public String importQuestionScoreTemplate(String termCode, String examId, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			scoreService.importQuestionScoreTemplate(termCode, "1", response);
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:" + adminPath + "/teacher/score/list?repage";
    }
	
	/**
	 * 导入科目试题成绩信息
	 * @param file
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:score:edit")
    @RequestMapping(value = "importQuestionScore", method=RequestMethod.POST)
    public String importQuestionScoreFile(String schoolCode, String termCode, MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			//解析excel
			String filename = file.getOriginalFilename();
			String examName = filename.substring(0, filename.indexOf("."));
			String names = filename.substring(filename.indexOf(".") + 1, filename.length());
			String examId = scoreService.getExamIdByName(examName);
			if (examId == null) {
				addMessage(redirectAttributes, "导入文件名称与考试名称不符！");
			} else {
				// 取得当前在用科目列表
				List<String> subList = scoreDao.getSubjects(schoolCode, termCode);
				// 取得当前在用科目列表并将科目名称和科目ID转化为K,V
				List<Subject> subjectList = scoreService.getSubjectLists(schoolCode, termCode);
				Map<String, String> map = new HashMap<String, String>();
				for (Subject subject : subjectList) {
					map.put(subject.getSubjectName(), subject.getSubjectId()+"");
				}
				String studentAccount = null;
				String subjectName = null;
				String subjectId = null;
				String questionId = null;
				String scoreId = null;
				ScoreSub score = new ScoreSub();
				for (int i = 0; i < subList.size(); i++) {
					//解析excel 如果失败更换其他方法
					InputStream is = file.getInputStream();
					List<Map<String, Object>> mapList = null;
					if ("xls".equals(names.toLowerCase())) {
						mapList = ResolveExcel.readXls(is);
					}
					if ("xlsx".equals(names.toLowerCase())) {
						mapList = ResolveExcel.readXlsx(is);
					}
					is.close();
					// 取得当前sheet页科目名称
					subjectName = subList.get(i);
					// 取得当前sheet页科目ID
					subjectId = scoreDao.getSubjectId(schoolCode, termCode, subjectName);
					for (Map<String, Object> rowMap : mapList){
						try{
							studentAccount = rowMap.get("学生账号").toString();
							Set<String> keySet = rowMap.keySet();
							keySet.remove("学生账号");
							keySet.remove("姓名");
							keySet.remove("rowIndex");
							scoreId = scoreDao.getScoreId("1", subjectId, studentAccount);
							for (String questionCode : keySet) {
								questionId = scoreDao.getQuestionId(examId, subjectId, questionCode);
								score.setScoreId(scoreId);
								score.setQuestionId(questionId);
								score.setScore(rowMap.get(questionCode).toString());
								scoreService.deleteQuestionScore(score);
								scoreService.insertQuestionScore(score);
							}
						}catch (Exception ex) {
							ex.printStackTrace();
							addMessage(redirectAttributes, "导入科目试题成绩信息失败！");
						}
					}
				}
				addMessage(redirectAttributes, "导入科目试题成绩信息成功！");
			}
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入科目试题成绩信息失败！");
		}
		return "redirect:" + adminPath + "/teacher/score/list?repage";
    }
	
	@RequiresPermissions("teacher:score:view")
	@RequestMapping(value = "getExamName", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	@ResponseBody
	public List<Exam> getExamName(@RequestBody String json) {
		Exam exam = JSON.parseObject(json, Exam.class);
		List<Exam> list = scoreService.getExamName(exam.getFirstYear(), exam.getTermCode());
		return list;
	}

}
