/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * @author songhao
 *
 */
public class Subject extends DataEntity<Subject> {

	private static final long serialVersionUID = 1L;
	
	private int subjectId;
	
	private String schoolCode;
	
	private String termCode;
	
	private String subjectClass;
	
	private String subjectCode;
	
	private String subjectName;
	
	private int fullScore;
	
	private String isAdvanced;
	
	private String isValid;

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getSubjectClass() {
		return subjectClass;
	}

	public void setSubjectClass(String subjectClass) {
		this.subjectClass = subjectClass;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getFullScore() {
		return fullScore;
	}

	public void setFullScore(int fullScore) {
		this.fullScore = fullScore;
	}

	public String getIsAdvanced() {
		return isAdvanced;
	}

	public void setIsAdvanced(String isAdvanced) {
		this.isAdvanced = isAdvanced;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}
}
