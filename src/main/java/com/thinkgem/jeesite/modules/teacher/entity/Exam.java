/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * @author paul
 *
 */
public class Exam extends DataEntity<Exam> {
	
	private static final long serialVersionUID = 1L;
	
	private String examId;
	
	private String schoolCode;
	
	private String firstYear;
	
	private String gradeCode;
	
	private String examDate;
	
	private String beginDate;
	
	private String endDate;
	
	private String examName;
	
	private String termCode;
	
	private String subjectCode;
	
	private String subjectId;
	
	private String subjectName;
	
	private String value;
	
	private Date examDateDate;
	
	private String subjects;
	
	@SuppressWarnings("unused")
	private List<String> subjectsList = new ArrayList<String>();

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public List<String> getSubjectList() {
		List<String> subjectList = Lists.newArrayList();
		if (subjects != null) {
			subjectList = Arrays.asList(subjects.split(","));
		}
		return subjectList;
	}

	public void setSubjectList(List<String> subjectList) {
		subjects = "";
		for (String subject : subjectList) {
			subjects += subject + ",";
		}
		subjects = subjects.substring(0,subjects.length()-1);
		this.subjectsList = subjectList;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExamDateDate() {
		if (examDate != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				examDateDate = df.parse(examDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return examDateDate;
	}

	public void setExamDateDate(Date examDateDate) {
		this.examDateDate = examDateDate;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getExamId() {
		return examId;
	}

	public void setExamId(String examId) {
		this.examId = examId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	public String getGradeCode() {
		return gradeCode;
	}

	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
