/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.web;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ImportExcel;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.teacher.entity.Class;
import com.thinkgem.jeesite.modules.teacher.entity.Student;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;
import com.thinkgem.jeesite.modules.teacher.service.StudentService;

/**
 * 学生Controller
 * @author songhao
 * @version 2018-3-29
 */
@Controller
@RequestMapping(value = "${adminPath}/teacher/student")
public class StudentController extends BaseController {

	@Autowired
	private StudentService studentService;
	
	@ModelAttribute
	public Student get(@RequestParam(required=false) String id) {
		if (StringUtils.isNotBlank(id)){
			return studentService.getStudent(id);
		}else{
			return new Student();
		}
	}
	
	@RequiresPermissions("teacher:student:view")
	@RequestMapping(value = {"list", ""})
	public String list(Student student, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Student> page = studentService.findStudent(new Page<Student>(request, response), student);
        model.addAttribute("page", page);
		return "modules/teacher/studentManage";
	}
	
	@RequiresPermissions("teacher:student:view")
	@RequestMapping(value = "form")
	public String form(Student student, Model model) {
		model.addAttribute("student", student);
		return "modules/teacher/studentForm";
	}
	
	@RequiresPermissions("teacher:student:edit")
	@RequestMapping(value = "delete")
	public String delete(Student student, RedirectAttributes redirectAttributes) {
		studentService.deleteStudent(student);
		studentService.deleteStudentScore(student);
		addMessage(redirectAttributes, "删除用户成功");
		return "redirect:" + adminPath + "/teacher/student/list?repage";
	}
	
	@RequiresPermissions("teacher:student:edit")
	@RequestMapping(value = "save")
	public String save(Student student, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		// 保存用户信息
		studentService.save(student);
		addMessage(redirectAttributes, "保存成功");
		return "redirect:" + adminPath + "/teacher/student/list?repage";
	}
	
	@RequiresPermissions("teacher:student:edit")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		if (ids != null) {
			String[] idArr = ids.split(",");
			Student stu = new Student();
			for (String id : idArr) {
				stu.setStudentId(id);
				studentService.deleteStudentScore(stu);
				studentService.deleteStudent(stu);
			}
		}
		addMessage(redirectAttributes, "批量删除成功");
		return "redirect:" + adminPath + "/teacher/student/list?repage";
	}
	
	/**
	 * 下载导入学生数据模板
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:student:view")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		try {
//            String fileName = "学生数据导入模板.xlsx";
//    		List<Student> list = Lists.newArrayList();
//    		// 获取样例数据
//    		
//    		new ExportExcel("学生数据", Student.class, 2).setDataList(list).write(response, fileName).dispose();
			//获取文件的路径
			String excelPath = request.getSession().getServletContext().getRealPath("/ExcelTemplate/学生数据导入模板.xls");
            // 读到流中
			InputStream inStream = new FileInputStream(excelPath);//文件的存放路径
            // 设置输出的格式
			response.reset();
			response.setContentType("bin");
			response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("学生数据导入模板.xls", "UTF-8"));
            // 循环取出流中的数据
			byte[] b = new byte[200];
			int len;
			while ((len = inStream.read(b)) > 0){
				response.getOutputStream().write(b, 0, len);
			}
			inStream.close();
			return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:" + adminPath + "/teacher/student/list?repage";
    }
	
	/**
	 * 导入学生信息
	 * @param file
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("teacher:student:edit")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		String filename = file.getOriginalFilename();
		String suffix = filename.substring(filename.indexOf(".")+1);
		if (suffix.toLowerCase().equals("xls") || suffix.toLowerCase().equals("xlsx")) {
			try {
				ImportExcel ei = new ImportExcel(file, 1, 0);
				List<Student> list = ei.getDataList(Student.class);
				
				// 循环插入学生信息
				Set<String> classSet = new HashSet<String>();
				for (Student student : list){
					classSet.add(student.getClassId());
					student.setFirstYear(student.getFirstYear());
					student.setClassId(student.getClassId());
					student.setStudentNo(student.getStudentAccount().substring(7, 9));
					student.setSex("男".equals(student.getSex())?"1":"0");
					studentService.deleteStudentByAccount(student);
					studentService.add(student);
					User user = new User();
					user.setLoginName(student.getStudentAccount());
					user.setName(student.getName());
					user.setNo(student.getStudentAccount().substring(5));
					user.setPassword(SystemService.entryptPassword(student.getStudentAccount()));
					studentService.deleteStudentUser(user);
					studentService.addUser(user);
				}
				
				// 取得班级信息并入库
				if (classSet.size() > 0) {
					Class info = new Class();
					String year = "20" + list.get(0).getStudentAccount().substring(3, 5);
					info.setFirstYear(list.get(0).getFirstYear());
					info.setSchoolCode(list.get(0).getStudentAccount().substring(0, 3));
					String className = "";
				    //当前年份
					int yearNow = Calendar.getInstance().get(Calendar.YEAR);
					int firstYear = Integer.parseInt(year);
					Date dt=new Date();
				    SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
			        //今年入学日期
				    Date date = sdf.parse(yearNow+"-08-25");
				    
				    // 删除之前班级数据
					studentService.deleteClass(list.get(0).getStudentAccount().substring(0, 3), year);
					for (String code : classSet) {
						info.setClassCode(code);
						//如果当前时间大于今年入学时间，今年入学的即为高1
					    if(sdf.format(dt).compareTo(sdf.format(date))>0){
					    	if ((yearNow - firstYear) <= 0) {
					    		className = "高一" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							} else if ((yearNow - firstYear) == 1) {
								className = "高二" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							} else {
								className = "高三" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							}
					    } else {
					    	if ((yearNow - firstYear) <= 1) {
								className = "高一" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							} else if ((yearNow - firstYear) == 2) {
								className = "高二" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							} else {
								className = "高三" + (code.substring(0, 1).equals("0")?code.substring(1, 2):code) + "班";
							}
					    }
						info.setClassName(className);
						studentService.addClass(info);
					}
				}
				addMessage(redirectAttributes, "导入学生信息成功!");
			} catch (Exception e) {
				e.printStackTrace();
				addMessage(redirectAttributes, "导入学生信息失败！");
			}
		} else {
			addMessage(redirectAttributes, "失败：导入文件格式不正确！");
		}
		return "redirect:" + adminPath + "/teacher/student/list?repage";
    }
	
	@RequiresPermissions("teacher:student:view")
	@RequestMapping(value = "getClass", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	@ResponseBody
	public List<StudentScore> getClass(@RequestBody String json) {
		Class classInfo = JSON.parseObject(json, Class.class);
		List<StudentScore> list = studentService.getClass(classInfo.getFirstYear());
		return list;
	}
	
}
