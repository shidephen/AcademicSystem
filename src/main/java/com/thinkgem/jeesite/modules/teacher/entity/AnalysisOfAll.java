package com.thinkgem.jeesite.modules.teacher.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class AnalysisOfAll extends DataEntity<AnalysisOfAll>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstYear;
	
	private String termCode;
	
	private String termName;
	
	private int examId;
	
	private String examName;
	
	private int subjectId;
	
	private String subjectName;
	
	private String allScore;
	
	private float averageScore;
	
	private float standardDeviation;
	
	private float passRate;
	
	private float excelLentrate;
	
	private String classCode;
	
	private String className;
	
	private int score60Low;
	
	private int score60;
	
	private int score70;
	
	private int score80;
	
	private int score90;
	
	private int score100;

	public int getScore60Low() {
		return score60Low;
	}

	public void setScore60Low(int score60Low) {
		this.score60Low = score60Low;
	}

	public int getScore60() {
		return score60;
	}

	public void setScore60(int score60) {
		this.score60 = score60;
	}

	public int getScore70() {
		return score70;
	}

	public void setScore70(int score70) {
		this.score70 = score70;
	}

	public int getScore80() {
		return score80;
	}

	public void setScore80(int score80) {
		this.score80 = score80;
	}

	public int getScore90() {
		return score90;
	}

	public void setScore90(int score90) {
		this.score90 = score90;
	}

	public int getScore100() {
		return score100;
	}

	public void setScore100(int score100) {
		this.score100 = score100;
	}

	public String getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getAllScore() {
		return allScore;
	}

	public void setAllScore(String allScore) {
		this.allScore = allScore;
	}

	public float getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(float averageScore) {
		this.averageScore = averageScore;
	}

	public float getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(float standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	public float getPassRate() {
		return passRate;
	}

	public void setPassRate(float passRate) {
		this.passRate = passRate;
	}

	public float getExcelLentrate() {
		return excelLentrate;
	}

	public void setExcelLentrate(float excelLentrate) {
		this.excelLentrate = excelLentrate;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
