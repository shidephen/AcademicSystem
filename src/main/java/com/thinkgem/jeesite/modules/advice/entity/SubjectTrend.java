package com.thinkgem.jeesite.modules.advice.entity;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class SubjectTrend extends DataEntity<SubjectTrend>{

	private static final long serialVersionUID = 1L;
	
	private List<Float> score;
	
	private List<String> scoreDate;
	
	private String subjectName;
	
	private String funl;

	private String start;

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	private String end;

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getFunl() {
		return funl;
	}

	public void setFunl(String funl) {
		this.funl = funl;
	}

	public List<Float> getScore() {
		return score;
	}

	public void setScore(List<Float> score) {
		this.score = score;
	}

	public List<String> getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(List<String> scoreDate) {
		this.scoreDate = scoreDate;
	}

}
