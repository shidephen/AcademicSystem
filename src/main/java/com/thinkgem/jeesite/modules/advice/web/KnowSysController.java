package com.thinkgem.jeesite.modules.advice.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreSearch;
import com.thinkgem.jeesite.modules.advice.service.StudentScoreService;

/**
 * @author yangping
 * 学生端--成绩查询
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/student/konwSys")
public class KnowSysController extends BaseController{
	

	@Autowired
	private StudentScoreService studentService;
	/**
	 * @return 考试
	 */
	@ResponseBody
	@RequestMapping(value = "getExam")
	public List<StudentScoreSearch> getExam(String termCode) {
		return this.studentService.getExam(termCode);
	}
	
	@RequestMapping(value="scoreSearch")
	public String scoreSearch(StudentScoreSearch search,HttpServletRequest request, HttpServletResponse response,Model model) {
		List<StudentScoreSearch> termList = this.studentService.getTerm();
		model.addAttribute("termList", termList);
		List<StudentScoreSearch> examList = this.studentService.getExam(termList.get(0).getTermCode());
		model.addAttribute("examList", examList);
		if(search.getExamId() == 0) {
			search.setExamId(examList.get(0).getExamId());
		}
		List<StudentScoreSearch> scoreList = this.studentService.getScore(search);
		model.addAttribute("scoreList",scoreList);
		model.addAttribute("allScore",scoreList.get(0).getAllScore());
		return 	"modules/student/knowSysList";
	}
}
