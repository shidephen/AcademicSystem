package com.thinkgem.jeesite.modules.advice.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * @author yangping
 * 选课建议entity
 *
 */
public class AdviceStudent extends DataEntity<AdviceStudent>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int studentAlias;

	private String studentAccount;
	
	private String name;
	
	private String firstYear;
	
	private float score;
	
	private String scoreDate;
	
	private String className;
	
	private int classId;

	private int examId;
	
	private String examName;
	
	private int comId;
	
	private int count;

	private int subjectId;

	private String subjectName;

	private int subjectId2;

	private String subjectName2;

	private int subjectId3;

	private String subjectName3;

	private float rLevel;

	private float cLevel;

	private float sSteady;

	private float cSteady;

	private float slope;

	private float intercept;

	private float stdDev;

	public int getStudentAlias() {
		return studentAlias;
	}

	public void setStudentAlias(int studentAlias) {
		this.studentAlias = studentAlias;
	}

	public String getStudentAccount() {
		return studentAccount;
	}

	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}

	public int getComId() {
		return comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getSubjectId2() {
		return subjectId2;
	}

	public void setSubjectId2(int subjectId2) {
		this.subjectId2 = subjectId2;
	}

	public String getSubjectName2() {
		return subjectName2;
	}

	public void setSubjectName2(String subjectName2) {
		this.subjectName2 = subjectName2;
	}

	public int getSubjectId3() {
		return subjectId3;
	}

	public void setSubjectId3(int subjectId3) {
		this.subjectId3 = subjectId3;
	}

	public String getSubjectName3() {
		return subjectName3;
	}

	public void setSubjectName3(String subjectName3) {
		this.subjectName3 = subjectName3;
	}

	public float getrLevel() {
		return rLevel;
	}

	public void setrLevel(float rLevel) {
		this.rLevel = rLevel;
	}

	public float getcLevel() {
		return cLevel;
	}

	public void setcLevel(float cLevel) {
		this.cLevel = cLevel;
	}

	public float getsSteady() {
		return sSteady;
	}

	public void setsSteady(float sSteady) {
		this.sSteady = sSteady;
	}

	public float getcSteady() {
		return cSteady;
	}

	public void setcSteady(float cSteady) {
		this.cSteady = cSteady;
	}

	public float getSlope() {
		return slope;
	}

	public void setSlope(float slope) {
		this.slope = slope;
	}

	public float getIntercept() {
		return intercept;
	}

	public void setIntercept(float intercept) {
		this.intercept = intercept;
	}

	public float getStdDev() {
		return stdDev;
	}

	public void setStdDev(float stdDev) {
		this.stdDev = stdDev;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public String getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(String scoreDate) {
		this.scoreDate = scoreDate;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
}
