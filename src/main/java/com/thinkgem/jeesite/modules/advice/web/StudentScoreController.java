package com.thinkgem.jeesite.modules.advice.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreSearch;
import com.thinkgem.jeesite.modules.advice.service.StudentScoreService;

/**
 * @author yangping
 * 学生端--成绩查询
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/student/scoreSearch")
public class StudentScoreController extends BaseController{
	

	@Autowired
	private StudentScoreService studentService;
	/**
	 * @return 考试
	 */
	@ResponseBody
	@RequestMapping(value = "getExam")
	public List<StudentScoreSearch> getExam(String termCode) {
		return this.studentService.getExam(termCode);
	}
	/**
	 * @return 科目
	 */
	@ResponseBody
	@RequestMapping(value = "getSubject")
	public List<StudentScoreSearch> getSubject(String termCode) {
		return this.studentService.getSubject(termCode);
	}
	/**
	 * 成绩查询
	 * @param search
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value="scoreSearch")
	public String scoreSearch(StudentScoreSearch search,HttpServletRequest request, HttpServletResponse response,Model model) {
		List<StudentScoreSearch> termList = this.studentService.getTerm();
		model.addAttribute("termList", termList);
		List<StudentScoreSearch> examList = this.studentService.getExam(termList.get(0).getTermCode());
		model.addAttribute("examList", examList);
		if(search.getExamId() == 0) {
			search.setExamId(examList.get(0).getExamId());
		}
		List<StudentScoreSearch> scoreList = this.studentService.getScore(search);
		model.addAttribute("scoreList",scoreList);
		model.addAttribute("allScore",scoreList.get(0).getAllScore());
		return 	"modules/student/scoreSearch";
	}
	
	/**
	 * 知识点分析
	 * @param search
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value="knowSys")
	public String konwSys(StudentScoreSearch search,HttpServletRequest request, HttpServletResponse response,Model model) {
		List<StudentScoreSearch> termList = this.studentService.getTerm();
		model.addAttribute("termList", termList);
		List<StudentScoreSearch> examList = this.studentService.getExam(termList.get(0).getTermCode());
		model.addAttribute("examList", examList);
		if(search.getExamId() == 0) {
			search.setExamId(examList.get(0).getExamId());
		}
		List<StudentScoreSearch> subjectList = this.studentService.getSubject(termList.get(0).getTermCode());
		model.addAttribute("subjectList", subjectList);
		model.addAttribute("knowList",this.studentService.getKnowSys(search));
		return 	"modules/student/knowSysList";
	}
	/**
	 * @return 成绩追踪
	 */
	@ResponseBody
	@RequestMapping(value = "follow")
	public Map<String,Object> follow(String key) {
		
		return this.studentService.follow(key);
	}
	
	/**
	 * 成绩追踪
	 * @param search
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value="scoreFollow")
	public String scoreFollow(StudentScoreSearch search,HttpServletRequest request, HttpServletResponse response,Model model) {
		List<StudentScoreSearch> termList = this.studentService.getTerm();
		List<StudentScoreSearch> subjectList = this.studentService.getSubject(termList.get(0).getTermCode());
		model.addAttribute("subjectList", subjectList);
		return 	"modules/student/scoreFollow";
	}
}
