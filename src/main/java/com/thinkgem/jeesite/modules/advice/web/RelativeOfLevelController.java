package com.thinkgem.jeesite.modules.advice.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thinkgem.jeesite.modules.advice.entity.SubjectPred;
import com.thinkgem.jeesite.modules.advice.service.AdviceService;

/**
 * @author yangping
 * 学科相对水平
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/advice/controller")
public class RelativeOfLevelController {
	
	@Autowired
	private AdviceService adviceService;
	
	/**
	 * @return 学科相对水平
	 */
	@ResponseBody
	@RequestMapping(value = "echart1")
	public Map<String,Object> echart1() {
		
		return this.adviceService.getRelativeLevel();
	}
	@RequestMapping(value="relative")
	public String relative() {
		return 	"modules/advice/relativeOfLevel";
	}
	
	/**
	 * @return 组合学科相对水平
	 */
	@ResponseBody
	@RequestMapping(value = "echart2")
	public Map<String,Object> echart2() {
		
		return this.adviceService.getComRelativeLevel();
	}
	
	@RequestMapping(value="comRelative")
	public String comRelative() {
		return 	"modules/advice/relativeComOfLevel";
	}
	
	/**
	 * @return 学科稳定性
	 */
	@ResponseBody
	@RequestMapping(value = "echart3")
	public Map<String,Object> echart3() {
		
		return this.adviceService.getStability();
	}
	@RequestMapping(value="subStability")
	public String subStability() {
		return 	"modules/advice/subjectStability";
	}
	/**
	 * @return 学科组合稳定性
	 */
	@ResponseBody
	@RequestMapping(value = "echart4")
	public Map<String,Object> echart4() {
		
		return this.adviceService.getComStability();
	}
	@RequestMapping(value="subComStability")
	public String subComStability() {
		return 	"modules/advice/subjectComStability";
	}
	/**
	 * @return 学科成绩走向
	 */
	@ResponseBody
	@RequestMapping(value = "echart5")
	public Map<String,Object> echart5() {
		
		return this.adviceService.getSubTrend();
	}
	@RequestMapping(value="subTrend")
	public String subTrend() {
		return 	"modules/advice/subjectTrend";
	}
	/**
	 * @return 组合选择预测
	 */
	@ResponseBody
	@RequestMapping(value = "echart6")
	public List<SubjectPred> echart6(Model model) {
		List<SubjectPred> list = this.adviceService.getComSubSelect();
		return list;
	}
	@RequestMapping(value="subComSelect")
	public String subComTrend() {
		return 	"modules/advice/subComSelect";
	}
	@RequestMapping(value="selComInflu")
	public String selComInflu() {
		return 	"modules/advice/selectComInflu";
	}
	@RequestMapping(value="analysisReport")
	public String analysisReport() {
		return 	"modules/advice/analysisReport";
	}
}
