package com.thinkgem.jeesite.modules.advice.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * @author yangping
 * 选课建议entity
 *
 */
public class AdviceSelect extends DataEntity<AdviceSelect>{

	private static final long serialVersionUID = 1L;
	
	private String studentAccount;
	
	private String id;
	
	private int examId;
	
	private String examDate;
	
	private int subjectId;
	
	private String subjectCode;
	
	private String subjectName;
	
	private int subjectScore;
	
	private int classOrder;
	
	private int gradeOrder;
	
	private float studentStandard;
	
	private float classStandard;
	
	private float gradeStandard;
	
	private int classLevel;
	
	private int gradeLevel;
	
	private String firstYear;
	
	private String subjectName2;
	
	private int subjectId2;
	
	private String subjectName3;
	
	private int subjectId3;
	
	private float zscore;
	
	private float zaverage;
	
	private int groupId;
	
	private String className;
	
	private int classid;
	
	public float getZscore() {
		return zscore;
	}

	public void setZscore(float zscore) {
		this.zscore = zscore;
	}

	public float getZaverage() {
		return zaverage;
	}

	public void setZaverage(float zaverage) {
		this.zaverage = zaverage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubjectName2() {
		return subjectName2;
	}

	public void setSubjectName2(String subjectName2) {
		this.subjectName2 = subjectName2;
	}

	public String getSubjectName3() {
		return subjectName3;
	}

	public void setSubjectName3(String subjectName3) {
		this.subjectName3 = subjectName3;
	}

	public float getStudentStandard() {
		return studentStandard;
	}

	public void setStudentStandard(float studentStandard) {
		this.studentStandard = studentStandard;
	}

	public float getClassStandard() {
		return classStandard;
	}

	public void setClassStandard(float classStandard) {
		this.classStandard = classStandard;
	}

	public float getGradeStandard() {
		return gradeStandard;
	}

	public void setGradeStandard(float gradeStandard) {
		this.gradeStandard = gradeStandard;
	}

	public int getClassLevel() {
		return classLevel;
	}

	public void setClassLevel(int classLevel) {
		this.classLevel = classLevel;
	}

	public int getGradeLevel() {
		return gradeLevel;
	}

	public void setGradeLevel(int gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	private int count;

	public String getStudentAccount() {
		return studentAccount;
	}

	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getSubjectScore() {
		return subjectScore;
	}

	public void setSubjectScore(int subjectScore) {
		this.subjectScore = subjectScore;
	}

	public int getClassOrder() {
		return classOrder;
	}

	public void setClassOrder(int classOrder) {
		this.classOrder = classOrder;
	}

	public int getGradeOrder() {
		return gradeOrder;
	}

	public void setGradeOrder(int gradeOrder) {
		this.gradeOrder = gradeOrder;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public int getSubjectId2() {
		return subjectId2;
	}

	public void setSubjectId2(int subjectId2) {
		this.subjectId2 = subjectId2;
	}

	public int getSubjectId3() {
		return subjectId3;
	}

	public void setSubjectId3(int subjectId3) {
		this.subjectId3 = subjectId3;
	}

}
