package com.thinkgem.jeesite.modules.advice.utils;

import java.util.Comparator;
import java.util.Map;

public class MapValueComparator2 implements Comparator<String> {

	Map<String, Object> base;
    public MapValueComparator2(Map<String, Object> base) {
        this.base = base;
    }
 
	@Override
	public int compare(String o1, String o2) {
		 if ((float)base.get(o1) <= (float)base.get(o2)) {
	            return -1;
	        } else {
	            return 1;
	        } 
	}
	
}