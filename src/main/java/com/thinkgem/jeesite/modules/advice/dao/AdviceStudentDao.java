package com.thinkgem.jeesite.modules.advice.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.advice.entity.AdviceStudent;

@MyBatisDao
public interface AdviceStudentDao extends CrudDao<AdviceStudent>{

	List<AdviceStudent> getComLevel(String studentAccount);

	List<AdviceStudent> getLevel(String studentAccount);

	List<AdviceStudent> getComSteady(String studentAccount);

	List<AdviceStudent> getSteady(String studentAccount);
	
	int getStudentCount(String studentAccount);
	
	List<AdviceStudent> getClevelByComId(int comId);
	
	List<AdviceStudent> getLevelBySubId(int subjectId);
	
	List<AdviceStudent> getScoreByAccount(String studentAccount);
	
	AdviceStudent getTrending(AdviceStudent studentAccount);
	
	List<AdviceStudent> getComBest();
	
	int getOrder(AdviceStudent student);
	
	List<AdviceStudent> getBestClass(int comId);
	
	List<String> getAdviceSub(String studentAccount);

	int getClassCount(String loginName);
	
	List<AdviceStudent> getClassName(@Param(value="studentAccount")String studentAccount) ;
	
	AdviceStudent getSubjectName(int comId);
	
	AdviceStudent getStudentInfo(String studentAccount);
	
	List<String> getBestSub(String studentAccount);
}
