package com.thinkgem.jeesite.modules.advice.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class StudentScoreFollow extends DataEntity<StudentScoreFollow>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String studentAccount;
	
	private float studentStandard;
	
	private String examDate;
	
	private int subjectId;
	
	public String getStudentAccount() {
		return studentAccount;
	}

	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}

	public float getStandardScore() {
		return studentStandard;
	}

	public void setStandardScore(float standardScore) {
		this.studentStandard = standardScore;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	

}
