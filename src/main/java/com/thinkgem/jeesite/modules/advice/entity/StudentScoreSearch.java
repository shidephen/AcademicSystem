package com.thinkgem.jeesite.modules.advice.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class StudentScoreSearch extends DataEntity<StudentScoreSearch>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String studentAccount;
	
	private String gradeCode;
	
	private String termCode;
	
	private String termName;
	
	private int examId;
	
	private String examName;
	
	private int allScore;
	
	private int subjectId;
	
	private String subjectName;
	
	private int subjectScore;
	
	private int classStandard;
	
	private int classLevel;
	
	private int gradeStandard;
	
	private int gradeLevel;

	public String getStudentAccount() {
		return studentAccount;
	}

	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public int getAllScore() {
		return allScore;
	}

	public void setAllScore(int allScore) {
		this.allScore = allScore;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getSubjectScore() {
		return subjectScore;
	}

	public void setSubjectScore(int subjectScore) {
		this.subjectScore = subjectScore;
	}

	public int getClassStandard() {
		return classStandard;
	}

	public void setClassStandard(int classStandard) {
		this.classStandard = classStandard;
	}

	public int getClassLevel() {
		return classLevel;
	}

	public void setClassLevel(int classLevel) {
		this.classLevel = classLevel;
	}

	public int getGradeStandard() {
		return gradeStandard;
	}

	public void setGradeStandard(int gradeStandard) {
		this.gradeStandard = gradeStandard;
	}

	public int getGradeLevel() {
		return gradeLevel;
	}

	public void setGradeLevel(int gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getGradeCode() {
		return gradeCode;
	}

	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}
	
}
