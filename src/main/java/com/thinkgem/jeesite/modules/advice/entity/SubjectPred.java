package com.thinkgem.jeesite.modules.advice.entity;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class SubjectPred extends DataEntity<SubjectPred>{

	private static final long serialVersionUID = 1L;
	
	private int groupId;
	
	private String className ;
	
	private int classid;
	
	private int zorder;
	
	private List<String> adviceSub;
	
	private String[] classList;
	
	private int[] classCount;
	
	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getClassPercent() {
		return classPercent;
	}

	public void setClassPercent(String classPercent) {
		this.classPercent = classPercent;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getPeoplePred() {
		return peoplePred;
	}

	public void setPeoplePred(int peoplePred) {
		this.peoplePred = peoplePred;
	}

	public int[] getCount() {
		return count;
	}

	public void setCount(int[] count) {
		this.count = count;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

	public List<String> getAdviceSub() {
		return adviceSub;
	}

	public void setAdviceSub(List<String> adviceSub) {
		this.adviceSub = adviceSub;
	}

	public int getZorder() {
		return zorder;
	}

	public void setZorder(int zorder) {
		this.zorder = zorder;
	}

	public String[] getClassList() {
		return classList;
	}

	public void setClassList(String[] classList) {
		this.classList = classList;
	}

	public int[] getClassCount() {
		return classCount;
	}

	public void setClassCount(int[] classCount) {
		this.classCount = classCount;
	}

	private String classPercent;
	
	private String subjectName;
	
	private int peoplePred;
	
	private int[] count;

}
