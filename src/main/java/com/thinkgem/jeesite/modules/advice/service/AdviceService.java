package com.thinkgem.jeesite.modules.advice.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.advice.dao.AdviceDao;
import com.thinkgem.jeesite.modules.advice.entity.AdviceSelect;
import com.thinkgem.jeesite.modules.advice.entity.SubjectPred;
import com.thinkgem.jeesite.modules.advice.utils.MapValueComparator;
import com.thinkgem.jeesite.modules.ans.LevelAnalyser;
import com.thinkgem.jeesite.modules.ans.NumericUtils;
import com.thinkgem.jeesite.modules.ans.SteadinessAnalyser;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 选课建议 service
 * @author yangping
 *
 */
@Service
@Transactional(readOnly = true)
public class AdviceService extends CrudService<AdviceDao, AdviceSelect>{
	
	@Autowired
	private AdviceDao adviceDao;
	
	public Map<String,Object> getRelativeLevel() {
		// 获取登陆账号 --->学生账号
		String loginName = UserUtils.getUser().getLoginName();
//		AdviceSelect param = new AdviceSelect();
//		param.setStudentAccount(loginName);
		//根据登陆账号获取相关学生的考试成绩信息
		List<AdviceSelect> allScores = this.adviceDao.AllstudentAllScore(loginName);
		if (allScores == null || allScores.size() ==0) {
			return null;
		}
		// 记录所有学生的每次考试所有学科的成绩
		Map<String, List<AdviceSelect>> allStuMap = new HashMap<String, List<AdviceSelect>>();
		
		String account = allScores.get(0).getStudentAccount();
		List<AdviceSelect> subCom =new ArrayList<AdviceSelect>();
		for (AdviceSelect adviceSelect : allScores) {
			if(account.equals(adviceSelect.getStudentAccount())) {
				subCom.add(adviceSelect);
			}else {
				allStuMap.put(account, subCom);
				subCom =new ArrayList<AdviceSelect>();
				account= adviceSelect.getStudentAccount();
				subCom.add(adviceSelect);
			}
		}
		allStuMap.put(account, subCom);
		
		List<AdviceSelect> countList = this.adviceDao.getSubCount(loginName);
		
		//所有相对水平
		Map<String, float[]> stuLevel = new HashMap<String,float[]>();
		Map<String, float[]> scoreRecordMap = new HashMap<String,float[]>();
		Map<String, String[]> subRecordMap = new HashMap<String,String[]>();
		
		Set<String> keys = allStuMap.keySet();
		for (String key : keys) {
			// 考试，学科记录
			float[] scoreRecord = new float[countList.size()];
			String[] subRecord = new String[countList.get(0).getCount()];
			float[][] scoreLevel = new float[countList.size()][countList.get(0).getCount()];
			
			int examId = allStuMap.get(key).get(0).getExamId();
			scoreRecord[0]=examId;
			int i = 0;
			int j = 0;
			for(AdviceSelect advice:allStuMap.get(key)) {
				
				if(examId == advice.getExamId()) {
					scoreLevel[i][j]= advice.getStudentStandard();
					if (!Arrays.asList(subRecord).contains(advice.getSubjectName())) {
						subRecord[j]=advice.getSubjectName();
					}
					
					j++;
				}else {
					i += 1;
					j = 0;
					scoreLevel[i][j]= advice.getStudentStandard();
					if (!Arrays.asList(subRecord).contains(advice.getSubjectName())) {
						subRecord[j]=advice.getSubjectName();
					}
					scoreRecord[i]=advice.getExamId();
					examId = advice.getExamId();
					j++;
				}
			}
			// 调用学科相对水平方法
			float[] relativeScore = LevelAnalyser.getRelativeLevel(scoreLevel);
			stuLevel.put(key, relativeScore);
			scoreRecordMap.put(key, scoreRecord);
			subRecordMap.put(key, subRecord);
		}
		
		float[] loginLevel = stuLevel.get(loginName);
		// 构造返回值
		Map<String,Object> resultMap = new HashMap<String,Object>();
		for(int j = 0;j<loginLevel.length;j++) {
			Set<String> kes = stuLevel.keySet();
			float[] subscore = new float[kes.size()];
			int perNum = 0;
			for (String key : kes) {
				subscore[perNum] = stuLevel.get(key)[j];
				perNum++;
			}
			resultMap.put(subRecordMap.get(loginName)[j], this.compareToAll(subscore, loginLevel[j]));
					
		}
		 // 结果排序
		 MapValueComparator bvc =  new MapValueComparator(resultMap);
	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
	     sorted_map.putAll(resultMap);
	     Set<String> sortKey = sorted_map.keySet();
	     String bestSub = "";
	     int sort = 0;
	     for (String string : sortKey) {
	    	 if(sort < 3) {
				bestSub += string + "、";
				sort++;
			}else if (sort == 3) {
				break;
			}
		}
		// 求全年级人数
		int count = this.adviceDao.getCount(loginName);
		resultMap.put("count", count);
		resultMap.put("best", bestSub);
		return resultMap;
		
	}
	
	// 排序进行大小比较
	private int compareToAll( float[] score,float level) {
		int num = 1;
		for (float f : score) {
			if(level < f) {
				num ++;
			}
		}
		return num;
	}
	/**
	 * 组合学科相对水平
	 * @return
	 */
	public Map<String, Object> getComRelativeLevel() {
		String loginName = UserUtils.getUser().getLoginName();
//		AdviceSelect param = new AdviceSelect();
//		param.setStudentAccount(loginName);
		//根据登陆账号获取相关学生的考试成绩信息
		List<AdviceSelect> allScores = this.adviceDao.AllstudentAllScore(loginName);
		if (allScores == null || allScores.size() ==0) {
			return null;
		}
		// 记录所有学生的每次考试所有学科的成绩
		Map<String, List<AdviceSelect>> allStuMap = new HashMap<String, List<AdviceSelect>>();
		
		String account = allScores.get(0).getStudentAccount();
		List<AdviceSelect> subCom =new ArrayList<AdviceSelect>();
		for (AdviceSelect adviceSelect : allScores) {
			if(account.equals(adviceSelect.getStudentAccount())) {
				subCom.add(adviceSelect);
			}else {
				allStuMap.put(account, subCom);
				subCom =new ArrayList<AdviceSelect>();
				account= adviceSelect.getStudentAccount();
				subCom.add(adviceSelect);
			}
		}
		allStuMap.put(account, subCom);
		List<AdviceSelect> suList = this.adviceDao.getSubNameAndId(loginName);
		int[][] subIds = new int[suList.size()][3];
		String[] subNames = new String[suList.size()];
		for(int ad = 0;ad<suList.size();ad++) {
			subIds[ad][0] = suList.get(ad).getSubjectId();
			subIds[ad][1] = suList.get(ad).getSubjectId2();
			subIds[ad][2] = suList.get(ad).getSubjectId3();
			subNames[ad] = suList.get(ad).getSubjectName() + suList.get(ad).getSubjectName2() + suList.get(ad).getSubjectName3() ;
		}
		
		List<AdviceSelect> countList = this.adviceDao.getSubCount(loginName);
		
		//所有相对水平
		Map<String, ArrayList<Float>> stuLevel = new HashMap<String,ArrayList<Float>>();
		Map<String, float[]> scoreRecordMap = new HashMap<String,float[]>();
		Map<String, String[]> subRecordMap = new HashMap<String,String[]>();
		
		Set<String> keys = allStuMap.keySet();
		for (String key : keys) {
			// 考试，学科记录
			float[] scoreRecord = new float[countList.size()];
			float[][] scoreLevel = new float[countList.size()][countList.get(0).getCount()];
			String[] subRecord = new String[countList.get(0).getCount()];
			int examId = allStuMap.get(key).get(0).getExamId();
			scoreRecord[0]=examId;
			int i = 0;
			int j = 0;
			for(AdviceSelect advice:allStuMap.get(key)) {
				
				if(examId == advice.getExamId()) {
					scoreLevel[i][j]= advice.getStudentStandard();
					if (!Arrays.asList(subRecord).contains(advice.getSubjectName())) {
						subRecord[j]=advice.getSubjectName();
					}
					j++;
				}else {
					i += 1;
					j = 0;
					scoreLevel[i][j]= advice.getStudentStandard();
					if (!Arrays.asList(subRecord).contains(advice.getSubjectName())) {
						subRecord[j]=advice.getSubjectName();
					}
					scoreRecord[i]=advice.getExamId();
					examId = advice.getExamId();
					j++;
				}
			}
			// 调用学科相对水平方法
			ArrayList<Float> relativeScore = LevelAnalyser.getCombinationLevel(scoreLevel, subIds);
			stuLevel.put(key, relativeScore);
			subRecordMap.put(key, subRecord);
			scoreRecordMap.put(key, scoreRecord);
		}
		ArrayList<Float> loginLevel = stuLevel.get(loginName);
		// 构造返回值
		Map<String,Object> resultMap = new HashMap<String,Object>();
		for(int j = 0;j<loginLevel.size();j++) {
			Set<String> kes = stuLevel.keySet();
			float[] subscore = new float[kes.size()];
			int perNum = 0;
			for (String key : kes) {
				subscore[perNum] = stuLevel.get(key).get(j);
				perNum++;
			}
			resultMap.put(subNames[j], this.compareToAll(subscore, loginLevel.get(j)));
					
		}
		// 结果排序
		 MapValueComparator bvc =  new MapValueComparator(resultMap);
	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
	     sorted_map.putAll(resultMap);
	     Set<String> sortKey = sorted_map.keySet();
	     String bestSub = "";
	     int sort = 0;
	     for (String string : sortKey) {
	    	 if(sort < 3) {
				bestSub += string + "、";
				sort++;
			}else if (sort == 3) {
				break;
			}
		}
		// 求全年级人数
		int count = this.adviceDao.getCount(loginName);
		resultMap.put("count", count);
		resultMap.put("best", bestSub);
		return resultMap;
	}
	/**
	 * 学生学科稳定性
	 * @return
	 */
	public Map<String, Object> getStability() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceSelect> allScores = this.adviceDao.AllstudentAllScore(loginName);
		if (allScores == null || allScores.size() ==0) {
			return null;
		}
		
		// 记录所有学生的每次考试所有学科的成绩
		Map<String, List<AdviceSelect>> allStuMap = new HashMap<String, List<AdviceSelect>>();
		
		String account = allScores.get(0).getStudentAccount();
		List<AdviceSelect> subCom =new ArrayList<AdviceSelect>();
		for (AdviceSelect adviceSelect : allScores) {
			if(account.equals(adviceSelect.getStudentAccount())) {
				subCom.add(adviceSelect);
			}else {
				allStuMap.put(account, subCom);
				subCom =new ArrayList<AdviceSelect>();
				account= adviceSelect.getStudentAccount();
				subCom.add(adviceSelect);
			}
		}
		allStuMap.put(account, subCom);
		//所有相对水平
		Map<String, float[]> stuLevel = new HashMap<String,float[]>();
		Map<String, float[]> scoreRecordMap = new HashMap<String,float[]>();
		Map<String, String[]> subRecordMap = new HashMap<String,String[]>();
		
		Set<String> keys = allStuMap.keySet();
		for (String key : keys) {

			
			// 考试，学科记录
			List<AdviceSelect> countList = this.adviceDao.getSubCount(loginName);
			float[] scoreRecord = new float[countList.size()];
			String[] subRecord = new String[countList.get(0).getCount()];
			float[][] scoreLevel = new float[countList.size()][countList.get(0).getCount()];
			
			int examId = allStuMap.get(key).get(0).getExamId();
			scoreRecord[0]=examId;
			int i = 0;
			int j = 0;
			for(AdviceSelect advice:allStuMap.get(key)) {
				
				if(examId == advice.getExamId()) {
					scoreLevel[i][j]= advice.getStudentStandard();
					if (!Arrays.asList(subRecord).contains(advice.getSubjectName())) {
						subRecord[j]=advice.getSubjectName();
					}
					j++;
				}else {
					i += 1;
					j = 0;
					scoreLevel[i][j]= advice.getStudentStandard();
					scoreRecord[i]=advice.getExamId();
					examId = advice.getExamId();
					j++;
				}
			}
			// 调用学科标准差方法
			float[] relativeScore = SteadinessAnalyser.getScoresStd(scoreLevel);
			stuLevel.put(key, relativeScore);
			scoreRecordMap.put(key, scoreRecord);
			subRecordMap.put(key, subRecord);
			
		}
		
		Set<String> allStu = stuLevel.keySet();
		float[][] stability = new float[allStu.size()][stuLevel.get(loginName).length];
		int ix = 0;
		int index = 0;
		for (String string : allStu) {
			if (loginName.equals(string)) {
				index = ix;
			}
			stability[ix]=stuLevel.get(string);
			ix++;
		}
		
		// 调用学科稳定性方法
		ArrayList<float[]> stabiList = SteadinessAnalyser.getSubjectSteadiness(stability);
		
		// 获取登录用户的学科稳定性
		float[] loginSub =  stabiList.get(index);
		
		ImmutablePair<int[], float[]> sortedRank = NumericUtils.ArgSort(loginSub);
		int[] ranking = sortedRank.left; // 排名
		float[] yyy = new float[ranking.length];
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		for(int y = 0;y<ranking.length;y++) {
			yyy[y] = ranking[y];
		}
		
		ImmutablePair<int[], float[]> sorted = NumericUtils.ArgSortWithoutRank(yyy);
		int[] sortLeft = sorted.left;//原来索引
		List<String> xValue = new ArrayList<String>();
		List<Float> yValue = new ArrayList<Float>();
		
		float[] compareToOne = new float[sortLeft.length];
		// 科目自己与自己比较的排名
		for(int y = 0;y<sortLeft.length;y++) {
			int xy = sortLeft[y];
			compareToOne[y] = ranking[y];
			resultMap.put(subRecordMap.get(loginName)[xy], ranking[y]);
		}
		ImmutablePair<int[], float[]> sortedOne = NumericUtils.ArgSort(compareToOne);
		int[] sortedOneRank = sortedOne.left; // 排名
		float[] sortedOneValue = sortedOne.right;
		for(int y = 0;y<sortedOneRank.length;y++) {
			int xy = sortedOneRank[y];
			xValue.add(subRecordMap.get(loginName)[xy]);
			yValue.add(sortedOneValue[y]);
			resultMap.put(subRecordMap.get(loginName)[xy], sortedOneValue[y]);
		}
		// 对排序结果进行排序，主图从高到低
//		for(int y = 0;y<loginSub.length;y++) {
//			resultMap.put(subRecordMap.get(loginName)[y], loginSub[y]);
//		}
		// 结果排序
//		 MapValueComparator2 bvc =  new MapValueComparator2(resultMap);
//	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
//	     sorted_map.putAll(resultMap);
//	     Set<String> sortKey = sorted_map.keySet();
//	     String bestSub = "";
//	     int sort = 0;
//	     for (String string : sortKey) {
//	    	 if(sort < 3) {
//				bestSub += string + "、";
//				sort++;
//			}else if (sort == 3) {
//				break;
//			}
//		}
		String bestSub = "";
		int sort = 0;
		for (int y = xValue.size()-1;y>=0;y--) {
			if(sort < 3) {
				bestSub += xValue.get(y) + "、";
				sort++;
			}
		}
		// 求全年级人数
		int count = this.adviceDao.getCount(loginName);
		resultMap.put("count", count);
		resultMap.put("best", bestSub);
		resultMap.put("x", xValue);
		resultMap.put("y", yValue);
		return resultMap;
	}
	
	/**
	 * 学生学科组合稳定性
	 * @return
	 */
	public Map<String, Object> getComStability() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceSelect> allScores = this.adviceDao.AllstudentAllScore(loginName);
		if (allScores == null || allScores.size() ==0) {
			return null;
		}
		
		// 记录所有学生的每次考试所有学科的成绩
		Map<String, List<AdviceSelect>> allStuMap = new HashMap<String, List<AdviceSelect>>();
		
		String account = allScores.get(0).getStudentAccount();
		List<AdviceSelect> subCom =new ArrayList<AdviceSelect>();
		for (AdviceSelect adviceSelect : allScores) {
			if(account.equals(adviceSelect.getStudentAccount())) {
				subCom.add(adviceSelect);
			}else {
				allStuMap.put(account, subCom);
				subCom =new ArrayList<AdviceSelect>();
				account= adviceSelect.getStudentAccount();
				subCom.add(adviceSelect);
			}
		}
		allStuMap.put(account, subCom);
		//所有相对水平
		// 考试，学科记录
		List<AdviceSelect> countList = this.adviceDao.getSubCount(loginName);
		Map<String, float[]> stuLevel = new HashMap<String,float[]>();
		Map<String, float[]> scoreRecordMap = new HashMap<String,float[]>();
		
		Set<String> keys = allStuMap.keySet();
		for (String key : keys) {
			
			float[] scoreRecord = new float[countList.size()];
			float[][] scoreLevel = new float[countList.size()][countList.get(0).getCount()];
			
			int examId = allStuMap.get(key).get(0).getExamId();
			scoreRecord[0]=examId;
			int i = 0;
			int j = 0;
			for(AdviceSelect advice:allStuMap.get(key)) {
				
				if(examId == advice.getExamId()) {
					scoreLevel[i][j]= advice.getSubjectScore();
					j++;
				}else {
					i += 1;
					j = 0;
					scoreLevel[i][j]= advice.getSubjectScore();
					scoreRecord[i]=advice.getExamId();
					examId = advice.getExamId();
					j++;
				}
			}
			// 调用学科标准差方法
			float[] relativeScore = SteadinessAnalyser.getScoresStd(scoreLevel);
			stuLevel.put(key, relativeScore);
			scoreRecordMap.put(key, scoreRecord);
		}
		
		Set<String> allStu = stuLevel.keySet();
		float[][] stability = new float[allStu.size()][stuLevel.get(loginName).length];
		int ix = 0;
		int index = 0;
		for (String string : allStu) {
			if (loginName.equals(string)) {
				index = ix;
			}
			stability[ix]=stuLevel.get(string);
			ix++;
		}
		
		// 调用学科稳定性方法
		ArrayList<float[]> stabiList = SteadinessAnalyser.getSubjectSteadiness(stability);
		float[][] comStab = new float[stabiList.size()][stabiList.get(0).length];
		for (int xy = 0;xy<stabiList.size();xy++) {
			comStab[xy] = stabiList.get(xy);
		}
		
		List<AdviceSelect> suList = this.adviceDao.getSubNameAndId(loginName);
		int[][] subIds = new int[suList.size()][3];
		String[] subNames = new String[suList.size()];
		for(int ad = 0;ad<suList.size();ad++) {
			subIds[ad][0] = suList.get(ad).getSubjectId();
			subIds[ad][1] = suList.get(ad).getSubjectId2();
			subIds[ad][2] = suList.get(ad).getSubjectId3();
			subNames[ad] = suList.get(ad).getSubjectName() + suList.get(ad).getSubjectName2() + suList.get(ad).getSubjectName3() ;
		}
		// 调用组合学科稳定性方法
		ArrayList<float[]> stabiComList = SteadinessAnalyser.getCombinedSteadiness(comStab,subIds);
		float[] loginSub = stabiComList.get(index);
		ImmutablePair<int[], float[]> sorted = NumericUtils.ArgSortWithoutRank(loginSub);
		int[] sortIndex = sorted.left;
		float[] sortValue = sorted.right;
		// 构造返回值
		Map<String,Object> resultMap = new HashMap<String,Object>();
//		for(int y = 0;y<loginSub.length;y++) {
//			resultMap.put(subNames[y], loginSub[y]);
//		}
		List<Float> yValue = new ArrayList<Float>();
		List<String> xValue = new ArrayList<String>();
		for(int y = 0;y<sortIndex.length;y++) {
			int xxx = sortIndex[y];
			xValue.add(subNames[xxx]);
			yValue.add(sortValue[y]);
			resultMap.put(subNames[xxx], sortValue[y]);
		}
//		// 结果排序
//		 MapValueComparator2 bvc =  new MapValueComparator2(resultMap);
//	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
//	     sorted_map.putAll(resultMap);
//	     Set<String> sortKey = sorted_map.keySet();
//	     String bestSub = "";
//	     int sort = 0;
//	     for (String string : sortKey) {
//	    	 if(sort < 3) {
//				bestSub += string + "、";
//				sort++;
//			}else if (sort == 3) {
//				break;
//			}
//		}
		String bestSub = "";
		int sort = 0;
		for (int y = xValue.size()-1;y>=0;y--) {
			if(sort < 3) {
				bestSub += xValue.get(y) + "、";
				sort++;
			}
		}
		// 求全年级人数
		int count = this.adviceDao.getCount(loginName);
		resultMap.put("count", count);
		resultMap.put("best", bestSub);
		resultMap.put("x", xValue);
		resultMap.put("y", yValue);
		return resultMap;
	}
	
	
	/**
	 * 学科成绩走向
	 */
	public Map<String, Object> getSubTrend() {
		// 获取登陆账号 --->学生账号
		String loginName = UserUtils.getUser().getLoginName();
//		AdviceSelect param = new AdviceSelect();
//		param.setStudentAccount(loginName);
		//根据登陆账号获取相关学生的考试成绩信息
		List<AdviceSelect> allScores = this.adviceDao.studentAllScore(loginName);
		
		// 考试次数，学科个数
		List<AdviceSelect> countList = this.adviceDao.getSubCount(loginName);
		float[] scoreRecord = new float[countList.size()];//考试次数
		String[] scoreDate = new String[countList.size()];
		
		//考试次数，考试时间
		int examId = allScores.get(0).getExamId();
		scoreRecord[0]=examId;
		scoreDate[0]=allScores.get(0).getExamDate();
		int i = 0;
		
		for(AdviceSelect advice:allScores) {
			if(examId == advice.getExamId()) {
			}else {
				i++;
				scoreDate[i] = advice.getExamDate();
				scoreRecord[i]=advice.getExamId();
				examId = advice.getExamId();
			}
			
		}
		// 考试分数
		Map<String, Object> subMap = new HashMap<String, Object>();
		for(AdviceSelect advice:allScores) {
			String subName = advice.getSubjectName();
			String sub = advice.getSubjectCode();
			float[] score = new float[countList.size()];
			int j=0;
			for (AdviceSelect advice2: allScores) {
				if(sub.equals(advice2.getSubjectCode())) {
					score[j] = advice2.getStudentStandard();
					j++;
				}
			}
			subMap.put(subName, score);
		}
		
		
		//循环调用方法求斜率和k值
		Set<String> keys = subMap.keySet();
		
		Map<String, float[]> kMap = new HashMap<String,float[]>();
		String subBest = "";
		for (String key : keys) {
			float[] y = (float[])subMap.get(key);
			float[] kValue = NumericUtils.LinearRegression(scoreRecord, y);
			if (kValue[0] >0) {
				subBest = subBest +key +",";
			}
			kMap.put(key, kValue);
	     }
		
		subMap.put("scoretime", scoreDate);
		subMap.put("best", subBest);
		// 求全年级人数
		int count = this.adviceDao.getCount(loginName);
		subMap.put("count", count);
		return subMap;
	}
	
	private int getIndex(int code) {
		String loginName = UserUtils.getUser().getLoginName();
		List<Integer> className = this.adviceDao.getClassName(loginName);
		for(int i=0;i<className.size();i++) {
			if(code == className.get(i)) {
				return i;
			}
		}
		return 0;
	}
	/**
	 * 组合选择预测
	 * @return
	 */
	public List<SubjectPred> getComSubSelect() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceSelect> list = this.adviceDao.getComSubPred();
		if (list == null || list.size() == 0) {
			return null;
		}
		SubjectPred subjectPred = new SubjectPred();
		List<SubjectPred> resultList = new ArrayList<SubjectPred>();
		
		//获取班级数目,班级id
		int classCount = this.adviceDao.getClassCount(loginName);
		
		int groupId = list.get(0).getGroupId();
		AdviceSelect subCom = this.adviceDao.getComSubject(groupId);
		int[] countList = new int[classCount];
		subjectPred.setSubjectName(subCom.getSubjectName()+subCom.getSubjectName2()+subCom.getSubjectName3());
		int count = 0;
		List<String> adviceSub = this.adviceDao.getAdviceSub(loginName);
		subjectPred.setAdviceSub(adviceSub);
		// 求全年级总人数
		int allCount = this.adviceDao.getCount(loginName);
		for(AdviceSelect advice:list) {
			if (advice.getGroupId() == groupId) {
				count += advice.getCount();
				int index = getIndex(advice.getClassid());
				countList[index] = advice.getCount();
			}else {
				DecimalFormat df = new DecimalFormat("0.0");
				String s = df.format((float)count/allCount *100) +"%";
				subjectPred.setClassPercent(s);
				subjectPred.setPeoplePred(count);
				subjectPred.setCount(countList);
				resultList.add(subjectPred);
				
				subjectPred = new SubjectPred();
				subCom = new AdviceSelect();
				groupId = advice.getGroupId();
				count = advice.getCount();
				countList = new int[classCount];
				int index = getIndex(advice.getClassid());
				countList[index] = advice.getCount();
				subCom = this.adviceDao.getComSubject(groupId);
				subjectPred.setSubjectName(subCom.getSubjectName()+subCom.getSubjectName2()+subCom.getSubjectName3());
			}
		}
		DecimalFormat df = new DecimalFormat("0.0");
		String s = df.format((float)count/allCount *100) +"%";
		subjectPred.setClassPercent(s);
		subjectPred.setPeoplePred(count);
		subjectPred.setCount(countList);
		resultList.add(subjectPred);
		return resultList;
	}
}
