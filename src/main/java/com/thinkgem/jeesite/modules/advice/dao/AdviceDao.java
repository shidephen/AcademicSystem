package com.thinkgem.jeesite.modules.advice.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.advice.entity.AdviceSelect;

@MyBatisDao
public interface AdviceDao extends CrudDao<AdviceSelect>{

	/**
	 * 根据学生账号查询学生所有考试的所有科目的考试成绩
	 * @param acount
	 * @return
	 */
	public List<AdviceSelect> studentAllScore(@Param(value="studentAccount")String studentAccount) ;
	/**
	 * 根据学生账号查询所有学生所有考试的所有科目的考试成绩
	 * @param acount
	 * @return
	 */
	public List<AdviceSelect> AllstudentAllScore(@Param(value="studentAccount")String studentAccount) ;
	
	/**
	 * 根据学生账号查询学生所有考试的所有科目的组合考试成绩
	 * @param acount
	 * @return
	 */
	public List<AdviceSelect> studentAllComScore(@Param(value="studentAccount")String studentAccount) ;
	/**
	 * 根据学生账号查询所有学生所有考试的所有科目的组合考试成绩
	 * @param studentAccount
	 * @return
	 */
	public List<AdviceSelect> allStudentAllComScore(@Param(value="studentAccount")String studentAccount) ;
	
	/**
	 * 根据学生组合科目ID和名称
	 * @param studentAccount
	 * @return
	 */
	public List<AdviceSelect> getSubNameAndId(@Param(value="studentAccount")String studentAccount) ;
	
	/**
	 * 求全年级人数
	 * @return
	 */
	public int getCount(@Param(value="studentAccount")String studentAccount) ;
	
	/**
	 * 获取班级数量
	 * @param studentAccount
	 * @return
	 */
	public int getClassCount(@Param(value="studentAccount")String studentAccount) ;
	/**
	 * 获取班级id
	 * @param studentAccount
	 * @return
	 */
	public List<Integer> getClassName(@Param(value="studentAccount")String studentAccount) ;
	
	/**
	 * 获取学校优势科目
	 * @param studentAccount
	 * @return
	 */
	public List<String> getAdviceSub(@Param(value="studentAccount")String studentAccount);
	
	public List<AdviceSelect> getSubCount(@Param(value="studentAccount")String studentAccount);
	
	public List<AdviceSelect> getComSubCount(@Param(value="studentAccount")String studentAccount);
	
	/**
	 * 学科预测
	 */
	public List<AdviceSelect> getComSubPred();
	
	/**
	 * 根据组合id获取组合科目名称
	 * @param id
	 * @return
	 */
	public AdviceSelect getComSubject(@Param(value="id") int id);
}
