package com.thinkgem.jeesite.modules.advice.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreFollow;

@MyBatisDao
public interface StudentScoreFollowDao extends CrudDao<StudentScoreFollow>{
	
	List<StudentScoreFollow> getFollowBySubject(StudentScoreFollow follow);
	List<StudentScoreFollow> getFollowByAll(StudentScoreFollow follow);
}
