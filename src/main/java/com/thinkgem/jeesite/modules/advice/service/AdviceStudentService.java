package com.thinkgem.jeesite.modules.advice.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.advice.dao.AdviceStudentDao;
import com.thinkgem.jeesite.modules.advice.entity.AdviceStudent;
import com.thinkgem.jeesite.modules.advice.entity.SubjectPred;
import com.thinkgem.jeesite.modules.advice.entity.SubjectTrend;
import com.thinkgem.jeesite.modules.advice.utils.MapValueComparator;
import com.thinkgem.jeesite.modules.ans.NumericUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 选课建议 service
 * @author yangping
 *
 */
@Service
@Transactional(readOnly = true)
public class AdviceStudentService extends CrudService<AdviceStudentDao, AdviceStudent>{
	
	@Autowired
	private AdviceStudentDao adviceStudentDao;
	
	private int getIndex(String[] account,String loginName) {
		for (int i = 0; i < account.length; i++) {
			if(loginName.equals(account[i])) {
				return i ;
			}
		}
		return 0;
	}
	public Map<String,Object> getRelativeLevel() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> list = this.adviceStudentDao.getLevel(loginName);
		Map<String,Object> returnMap = new HashMap<String,Object>();
		for (AdviceStudent adviceStudent : list) {
			String subjectName = adviceStudent.getSubjectName();
			List<AdviceStudent> subList = this.adviceStudentDao.getLevelBySubId(adviceStudent.getSubjectId());
			String[] account = new String[subList.size()];
			float[] cLevel = new float[subList.size()];
			for (int i = 0; i < subList.size(); i++) {
				account[i] = subList.get(i).getStudentAccount();
				cLevel[i] = subList.get(i).getcLevel();
			}
			ImmutablePair<int[],float[]> table = NumericUtils.ArgSort(cLevel);
			int index =getIndex(account, loginName);
			int[] ranking = table.left;
			returnMap.put(subjectName, ranking[index]);
		}
		returnMap.put("count", this.adviceStudentDao.getStudentCount(loginName));
		// 结果排序
		 MapValueComparator bvc =  new MapValueComparator(returnMap);
	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
	     sorted_map.putAll(returnMap);
	     Set<String> sortKey = sorted_map.keySet();
	     String bestSub = "";
	     int sort = 0;
	     for (String string : sortKey) {
	    	 if(sort < 3) {
				bestSub += string + "、";
				sort++;
			}else if (sort == 3) {
				break;
			}
		}
	    returnMap.put("best", bestSub);
		return returnMap;
	}

	/**
	 * 组合学科相对水平
	 * @return
	 */
	public Map<String, Object> getComRelativeLevel() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> list = this.adviceStudentDao.getComLevel(loginName);
		Map<String,Object> returnMap = new HashMap<String,Object>();
		for (AdviceStudent adviceStudent : list) {
			String subjectName = adviceStudent.getSubjectName().substring(0, 1) + adviceStudent.getSubjectName2().substring(0, 1) +adviceStudent.getSubjectName3().substring(0, 1);
			List<AdviceStudent> comList = this.adviceStudentDao.getClevelByComId(adviceStudent.getComId());
			String[] account = new String[comList.size()];
			float[] cLevel = new float[comList.size()];
			for (int i = 0; i < comList.size(); i++) {
				account[i] = comList.get(i).getStudentAccount();
				cLevel[i] = comList.get(i).getcLevel();
			}
			ImmutablePair<int[],float[]> table = NumericUtils.ArgSort(cLevel);
			int index =getIndex(account, loginName);
			int[] ranking = table.left;
			returnMap.put(subjectName, ranking[index]);
		}
		// 结果排序
		 MapValueComparator bvc =  new MapValueComparator(returnMap);
	     TreeMap<String,Object> sorted_map = new TreeMap<String,Object>(bvc);
	     sorted_map.putAll(returnMap);
	     Set<String> sortKey = sorted_map.keySet();
	     String bestSub = "";
	     int sort = 0;
//	     int sss = 0;
//	     for (String string : sortKey) {
//	    	 if(sort == 0) {
//	    	 	sss = (int)returnMap.get(string);
//				bestSub += string + "、";
//				sort++;
//			}else{
//	    	 	if (sss== (int)returnMap.get(string)){
//					bestSub += string + "、";
//				}
//			 }
//		}
	     for (String string : sortKey) {
	    	 if(sort < 3) {
				bestSub += string + "、";
				sort++;
			}else if (sort == 3) {
				break;
			}
		}
		
		returnMap.put("count", this.adviceStudentDao.getStudentCount(loginName));
	    returnMap.put("best", bestSub);
		return returnMap;
	}
	/**
	 * 学生学科稳定性
	 * @return
	 */
	public Map<String, Object> getStability() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> list = this.adviceStudentDao.getSteady(loginName);
		String[] subName = new String[list.size()];
		float[]  comSta = new float[list.size()];
		for (int i = 0; i < list.size(); i++) {
			subName[i] = list.get(i).getSubjectName();
			comSta[i] = list.get(i).getsSteady();
		}
		ImmutablePair<int[], float[]> sTable = NumericUtils.ArgSortWithoutRank(comSta);
		int[] index = sTable.left;
		float[] y = sTable.right;
		String[] x = new String[index.length];

		for (int i = 0; i < index.length; i++) {
			int ss = index[i];
			x[i] = subName[ss];
		}
		Map<String, Object> returnMap = new HashMap<String,Object>();
		returnMap.put("x", x);
		returnMap.put("y",y);
		returnMap.put("count", this.adviceStudentDao.getStudentCount(loginName));
		String bestSub = "";
		int sort = 0;
		for (int r = x.length-1;r>=0;r--) {
			if(sort < 3) {
				bestSub += x[r] + "、";
				sort++;
			}
		}
		returnMap.put("best", bestSub);
		return returnMap;
	}
	
	/**
	 * 学生学科组合稳定性
	 * @return
	 */
	public Map<String, Object> getComStability() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> list = this.adviceStudentDao.getComSteady(loginName);
		String[] subName = new String[list.size()];
		float[]  comSta = new float[list.size()];
		for (int i = 0; i < list.size(); i++) {
			subName[i] = list.get(i).getSubjectName().substring(0, 1)+list.get(i).getSubjectName2().substring(0, 1)+list.get(i).getSubjectName3().substring(0, 1);
			comSta[i] = list.get(i).getcSteady();
		}
		ImmutablePair<int[], float[]> sTable = NumericUtils.ArgSortWithoutRank(comSta);
		int[] index = sTable.left;
		float[] y = sTable.right;
		String[] x = new String[index.length];

		for (int i = 0; i < index.length; i++) {
			int ss = index[i];
			x[i] = subName[ss];
		}
		
		String bestSub = "";
//		int sort = x.length-1;
//		float sss = 0;
//		for (int r = x.length-1;r>=0;r--) {
//			if(sort == x.length-1) {
//				sss = y[r];
//				bestSub += x[r] + "、";
//				sort++;
//			}else{
//				if (sss ==  y[r]){
//					bestSub += x[r] + "、";
//				}
//			}
//		}
		int sort = 0;
		for (int r = x.length-1;r>=0;r--) {
		if(sort < 3) {
			bestSub += x[r] + "、";
			sort++;
		}
	}
		Map<String, Object> returnMap = new HashMap<String,Object>();
		returnMap.put("x", x);
		returnMap.put("y",y);
		returnMap.put("count", this.adviceStudentDao.getStudentCount(loginName));
		returnMap.put("best", bestSub);
		return returnMap;
	}
	
	
	/**
	 * 学科成绩走向
	 */
	public Map<String, Object> getSubTrend() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> list = this.adviceStudentDao.getScoreByAccount(loginName);
		int subjectId = list.get(0).getSubjectId();
		List<String> scoreTime = new ArrayList<String>();
		//考试次数，考试时间
		for(AdviceStudent advice:list) {
			if(subjectId == advice.getSubjectId()) {
				scoreTime.add(advice.getScoreDate().substring(0, 7));
			}
			
		}
		Map<String, Object> subMap = new HashMap<String, Object>();
		String subName = list.get(0).getSubjectName();
		int subId = list.get(0).getSubjectId();
		List<Float> score = new ArrayList<Float>();
		for (AdviceStudent adviceStudent : list) {
			if(subName.equals(adviceStudent.getSubjectName())) {
				score.add(adviceStudent.getScore());
				
			}else {
				SubjectTrend trend =  new SubjectTrend();
				trend.setScore(score);
				AdviceStudent param = new AdviceStudent();
				param.setStudentAccount(loginName);
				param.setSubjectId(subId);
				AdviceStudent resultTrend = this.adviceStudentDao.getTrending(param);

				String zhi  = "";
				if (String.valueOf(resultTrend.getIntercept()).indexOf("-")>=0){
					 zhi  = "y="+resultTrend.getSlope()+" * x "+resultTrend.getIntercept();
				}else {
					zhi  = "y="+resultTrend.getSlope()+" * x + "+resultTrend.getIntercept();
				}
				trend.setFunl(zhi);
				trend.setStart(String.valueOf(resultTrend.getSlope() * 1  + resultTrend.getIntercept()));
				trend.setEnd(String.valueOf(resultTrend.getSlope() * scoreTime.size()  + resultTrend.getIntercept()));
				subMap.put(subName, trend);
				subName = adviceStudent.getSubjectName();
				subId = adviceStudent.getSubjectId();
				score = new ArrayList<Float>();
				score.add(adviceStudent.getScore());
			}
		}
		SubjectTrend trend =  new SubjectTrend();
		trend.setScore(score);
		AdviceStudent param = new AdviceStudent();
		param.setStudentAccount(loginName);
		param.setSubjectId(subId);
		AdviceStudent resultTrend = this.adviceStudentDao.getTrending(param);
		String zhi = "";
		if (String.valueOf(resultTrend.getIntercept()).indexOf("-")>=0){
			 zhi  = "y="+resultTrend.getSlope()+" * x "+resultTrend.getIntercept();
		}else {
			zhi  = "y="+resultTrend.getSlope()+" * x + "+resultTrend.getIntercept();
		}
		trend.setFunl(zhi);
		trend.setStart(String.valueOf(resultTrend.getSlope() * 1  + resultTrend.getIntercept()));
		trend.setEnd(String.valueOf(resultTrend.getSlope() * scoreTime.size()  + resultTrend.getIntercept()));
		subMap.put(subName, trend);
		subMap.put("count", this.adviceStudentDao.getStudentCount(loginName));
		subMap.put("scoreTime", scoreTime);

		String bestSub = "";
		List<String> bestList = this.adviceStudentDao.getBestSub(loginName);
		int count = 0;
		for (int i = 0; i < bestList.size(); i++) {
			if(!"数学".equals(bestList.get(i))&&!"语文".equals(bestList.get(i))&&!"外语".equals(bestList.get(i))){
				bestSub += bestList.get(i) + "、";
				count++;
			}
			if (count == 3){
				break;
			}

		}
		subMap.put("best", bestSub);
		return subMap;
	}
	/**
	 * 组合选择预测
	 * @return
	 */
	public List<SubjectPred> getComSubSelect() {
		String loginName = UserUtils.getUser().getLoginName();
		List<AdviceStudent> subPredList = this.adviceStudentDao.getComBest();
		List<SubjectPred> resultList = new ArrayList<SubjectPred>();
		//获取年级人数
		int allCount = this.adviceStudentDao.getStudentCount(loginName);
		//获取班级数目,班级id
		int classNumber = this.adviceStudentDao.getClassCount(loginName);
		for (AdviceStudent adviceStudent: subPredList) {
			SubjectPred subjectPred = new SubjectPred();
			//学校推荐科目
			List<String> adviceSub = this.adviceStudentDao.getAdviceSub(loginName);
			subjectPred.setAdviceSub(adviceSub);
			AdviceStudent subObject = this.adviceStudentDao.getSubjectName(adviceStudent.getComId());
			subjectPred.setSubjectName(subObject.getSubjectName()+subObject.getSubjectName2()+subObject.getSubjectName3());
			subjectPred.setPeoplePred(adviceStudent.getCount());
			DecimalFormat df = new DecimalFormat("0.0");
			String s = df.format((float)adviceStudent.getCount()/allCount *100) +"%";
			subjectPred.setClassPercent(s);
			// 班级数和排名
			AdviceStudent  param = new AdviceStudent();
			param.setComId(adviceStudent.getComId());
			param.setStudentAccount(loginName);
			subjectPred.setZorder(this.adviceStudentDao.getOrder(param));
			// 各班级占比
			int[] classCountList = new int[classNumber];
			String[] classNameList = new String[classNumber];
			List<AdviceStudent> classList = this.adviceStudentDao.getBestClass(adviceStudent.getComId());
//			List<String> className = new ArrayList<String>();
//			List<Integer> classCount =  new ArrayList<Integer>();
			for (AdviceStudent adviceStudent2 : classList) {
				List<AdviceStudent> className = this.adviceStudentDao.getClassName(loginName);
				for(int i=0;i<className.size();i++) {
					if(adviceStudent2.getClassId() == className.get(i).getClassId()) {
						classCountList[i] = adviceStudent2.getCount();
					}
					classNameList[i] = className.get(i).getClassName();
				}
			}
			subjectPred.setClassList(classNameList);
			subjectPred.setClassCount(classCountList);
			resultList.add(subjectPred);
		}
		return resultList;
	}
	public AdviceStudent getStudentInfo() {
		String loginName =  UserUtils.getUser().getLoginName();
		AdviceStudent student =  adviceStudentDao.getStudentInfo(loginName);
		SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" ); 
        String str = sdf.format(new Date()); 
		student.setScoreDate(str);
		return student;
	}
}
