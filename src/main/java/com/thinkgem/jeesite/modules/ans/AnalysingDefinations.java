package com.thinkgem.jeesite.modules.ans;

public final class AnalysingDefinations {
    /**
     * 学科组合序号
     */
    public static final int[][] AllCombinations
            = new int[][]{
            {6, 7, 4},
            {6, 7, 5},
            {6, 8, 4},
            {6, 3, 4},
            {6, 4, 5},
            {6, 8, 3},
            {6, 8, 5},
            {6, 3, 5},
            {6, 7, 3},
            {6, 7, 8},
            {7, 8, 4},
            {7, 3, 4},
            {7, 8, 3},
            {7, 3, 5},
            {7, 4, 5},
            {7, 8, 5},
            {8, 4, 5},
            {8, 3, 5},
            {8, 3, 4},
            {3, 4, 5}
    };
}
