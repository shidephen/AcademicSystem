package com.thinkgem.jeesite.modules.ans.tasks;

import org.springframework.stereotype.Service;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.scheduling.annotation.Async;

import java.sql.Connection;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import com.thinkgem.jeesite.modules.ans.*;

/**
 * AssignmentAnaTask
 */
@Service
public class AssignmentAnaTask {
    DbUtils db_util;
    public AssignmentAnaTask(Connection conn){
        db_util = new DbUtils(conn);
    }

    public DbUtils getDbUtils(){
        return db_util;
    }

    @Async
    public CompletableFuture<ImmutablePair<int[], float[]>>
    ApplyDifficulty(int examId){
        int[] subjects = db_util.getSubjectsByExam(examId);
        float[][] scores = db_util.getScoresByExam(examId).right;
        
        HashMap<Integer, Float> map = db_util.getSubjectWithFullScore();

        float[] diffs = new float[subjects.length];
        float[] subscores = new float[scores.length];

        for(int i = 0; i < subjects.length; i++){
            Float fullscore = map.get(Integer.valueOf(subjects[i]));
            for(int j = 0; j < scores.length; j++)
                subscores[j] = scores[j][i];
            float d = AssignmentAnalyser.getDifficulty(
                subscores,
                fullscore.floatValue());
            diffs[i] = d;
        }

        db_util.saveDifficulties(examId, subjects, diffs);

        return CompletableFuture.completedFuture(
            new ImmutablePair<>(subjects, diffs));
    }
}
