package com.thinkgem.jeesite.modules.ans;

import org.apache.commons.math3.util.Precision;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;


/**
 * 试卷分析
 */
public class AssignmentAnalyser {
    private INDArray m_fullScores;
    private INDArray m_scores;
    public static int PRECISION = 2;
    private float total_score;

    /**
     *
     * @param fullScores N 道题的满分成绩
     */
    public AssignmentAnalyser(float fullScores[]) {
        m_fullScores = Nd4j.create(fullScores);
        m_scores = null;
        total_score = m_fullScores.sumNumber().floatValue();
    }

    /**
     *
     * @param fullScores N 道题的满分成绩
     * @param scores MxN 考试分数，M为学生数
     */
    public AssignmentAnalyser(float fullScores[], float scores[][]){
        this(fullScores);
        m_scores = Nd4j.create(scores);
    }

    /**
     * 设置考试成绩
     * @param scores MxN 考试分数，M为学生数，N为考题数
     */
    public void setScores(float scores[][]){
        m_scores = Nd4j.create(scores);
    }

    /**
     * 计算信度
     * @return
     */
    public float getReliability(){
        checkScore();
        
        INDArray Sj = m_scores.std(0);
        Sj = Sj.mul(Sj);
        float sum_sj = Sj.sumNumber().floatValue();
        float s = m_scores.sum(1).stdNumber().floatValue();
        s = s * s;
        float n = (float)m_scores.columns();
        float R = n / (n-1) * (1 - sum_sj/s);
        
        return Precision.round(R, PRECISION);
    }
    
    // UNDONE: 需要确认用哪个版本的难度计算
    /**
     * 计算难度
     * @return
     */
    public float getDifficulty(){
        checkScore();

        INDArray means = m_scores.mean(0);
        INDArray Dj = Nd4j.ones(m_scores.columns())
                .sub(means.div(m_fullScores));

        float D = 1 / total_score * Dj.mul(m_fullScores).sumNumber().floatValue();
        D = Precision.round(D, PRECISION);

        return D;
    }

    public static float getDifficulty(float[] scores, float full){
        INDArray S = Nd4j.create(scores);
        float mean = S.meanNumber().floatValue();
        return Precision.round(mean / full, PRECISION);
    }

    /**
     * 计算区分度
     * @param range 分组界限n，高n人为高分组，低n人为低分组
     * @return
     */
    public float getDifferentiation(int range){
        checkScore();
        if(range >= m_scores.rows())
            throw new IllegalArgumentException("Range must be less than quantity of students");

        int columnNum = m_scores.columns();
        int rowNum = m_scores.rows();
        final int num = 10;

        INDArray sorted_idx[] = Nd4j.sortWithIndices(
                Nd4j.sum(m_scores, 1), 0, false);

        INDArray sorted = m_scores.get(NDArrayIndex.create(sorted_idx[0]));
        INDArray hGroup = sorted.get(
                NDArrayIndex.interval(0, num), NDArrayIndex.all());
        INDArray lGroup = sorted.get(
                NDArrayIndex.interval(rowNum - num, rowNum), NDArrayIndex.all());

        INDArray Hj = Nd4j.zeros(columnNum);
        INDArray Lj = Nd4j.zeros(columnNum);
        for(int i = 0; i < columnNum; i++){
            INDArray hCol = hGroup.getColumn(i);
            INDArray lCol = lGroup.getColumn(i);
            Hj.putScalar(i, hCol.meanNumber().floatValue());
            Lj.putScalar(i, lCol.meanNumber().floatValue());
        }
        INDArray Taoj = Hj.sub(Lj).div(m_fullScores);

        float tao = 1 / total_score * Taoj.mul(m_fullScores).sumNumber().floatValue();
        return Precision.round(tao, PRECISION);

    }

    private void checkScore(){
        if(m_scores == null)
            throw new IllegalStateException("There are no scores here");
    }
}
