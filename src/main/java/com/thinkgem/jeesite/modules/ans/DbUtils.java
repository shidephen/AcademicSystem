package com.thinkgem.jeesite.modules.ans;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * DbUtils
 */
public class DbUtils {
    private Connection conn;
    private Logger logger;

    public DbUtils(Connection conn){
        this.conn = conn;
        logger = LoggerFactory.getLogger(getClass());
    }

    public ImmutablePair<int[], int[][]> getAllCombinations(){
        final String sql = "select * from as_subjectgroup order by subjectgroupid";
        Statement stmt = null; ResultSet rs = null;
        try{
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            
            rs.last();
            int num_comb = rs.getRow();
            if(num_comb == 0){
                logger.error("There'is no combinations in the database.");
                return null;
            }
            int[][] combines = new int[num_comb][3];
            int[] ids = new int[num_comb];
            rs.first();rs.previous();

            for(int i = 0; rs.next(); i++){
                ids[i] = rs.getInt(1);
                for(int j = 0; j < 3; j++)
                    combines[i][j] = rs.getInt(j+2);
            }

            return new ImmutablePair<>(ids, combines);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * getField
     * @param examId 获取当次考试的所有科目数
     */
    public int[] getSubjectsByExam(int examId){
        final String sql =
            "select distinct subjectid from as_score_sub where examid=? order by subjectid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, examId);
            rs = ptmt.executeQuery();

            rs.last();
            int n = rs.getRow();
            int[] subjects = new int[n];

            rs.first();rs.previous();
            for(int i = 0; rs.next(); i++){
                subjects[i] = rs.getInt(1);
            }

            return subjects;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int[] getSubjectsByStudent(int studentId){
        final String sql = 
            "select distinct AS_SCORE_SUB.SUBJECTID from AS_SCORE_SUB join AS_STUDENT on AS_STUDENT.STUDENTACCOUNT = AS_SCORE_SUB.STUDENTACCOUNT where AS_STUDENT.STUDENTID = ? order by subjectid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, studentId);
            rs = ptmt.executeQuery();

            rs.last();
            int n = rs.getRow();
            int[] subjects = new int[n];

            rs.first();rs.previous();
            for(int i = 0; rs.next(); i++){
                subjects[i] = rs.getInt(1);
            }
            return subjects;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取学科满分
     */
    public HashMap<Integer, Float>
    getSubjectWithFullScore(){
        final String sql = 
            "select subjectid, fullscore from as_subject order by subjectid";
        Statement stmt = null; ResultSet rs = null;
        try{
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);

            HashMap<Integer, Float> subjects = new HashMap<>();

            while(rs.next())
                subjects.put(
                    rs.getInt("subjectid"),
                    rs.getFloat("fullscore"));
            
            return subjects;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 通过学生ID获取考试ID
     */
    public int[] getExamIdsByStudent(int studentId){
        final String getExamState = 
            "select distinct examid from AS_SCORE_SUB join AS_STUDENT on AS_STUDENT.STUDENTACCOUNT=AS_SCORE_SUB.STUDENTACCOUNT where studentid=? order by examid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(getExamState, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, studentId);
            rs = ptmt.executeQuery();
            rs.last();
            int n = rs.getRow();
            int[] exams = new int[n];

            rs.first();rs.previous();
            for(int i = 0; rs.next(); i++){
                exams[i] = rs.getInt(1);
            }
            return exams;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int[] getStudentIdsByExam(int examId){
        final String sql = 
            "select distinct studentid from AS_SCORE_SUB join AS_STUDENT on AS_STUDENT.STUDENTACCOUNT=AS_SCORE_SUB.STUDENTACCOUNT where examid = ? order by studentid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, examId);
            rs = ptmt.executeQuery();

            rs.last();
            int num = rs.getRow();
            int[] students = new int[num];

            rs.first();rs.previous();
            for(int i = 0; rs.next(); i++)
                students[i] = rs.getInt(1);
            
            return students;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public float[][] getExamDiffsByStudent(int studentId){
        final String sql = 
            "select "+
            "AS_SCORE_SUB.examid,"+
            "AS_SCORE_SUB.subjectid,"+
            "paperdifficulty "+
            "from AS_SCORE_SUB "+
            "join AS_EXAMSUBJECT "+
            "on AS_SCORE_SUB.examid = AS_EXAMSUBJECT.examid "+
            "and AS_SCORE_SUB.subjectid = AS_EXAMSUBJECT.subjectid "+
            "join AS_STUDENT "+
            "on AS_STUDENT.STUDENTACCOUNT = AS_SCORE_SUB.STUDENTACCOUNT "+
            "where AS_STUDENT.studentid = ? "+
            "order by AS_SCORE_SUB.examid, AS_SCORE_SUB.subjectid";

        int[] subjects = getSubjectsByStudent(studentId);
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, studentId);
            rs = ptmt.executeQuery();

            rs.last();
            int total_rows = rs.getRow();
            int num_subject = subjects.length;
            int num_exam = total_rows / num_subject;
            
            rs.first();rs.previous();
            float[][] diff = new float[num_exam][num_subject];

            for(int i = 0; rs.next(); i++){
                int eidx = i / num_subject;
                int sidx = i % num_subject;
                diff[eidx][sidx] = rs.getFloat("paperdifficulty");
            }
            
            return diff;
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ImmutablePair<int[], float[][]> getScoresByExam(int examId){
        final String sql = 
            "select studentid, score from as_score_sub join AS_STUDENT on AS_STUDENT.STUDENTACCOUNT=AS_SCORE_SUB.STUDENTACCOUNT where examid=? order by studentid, subjectid";
        int subject_num = getSubjectsByExam(examId).length;

        if(subject_num == 0)
        {
            logger.error(String.format("There's no subject data for exam ({})", examId));
            return null;
        }

        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, examId);
            rs = ptmt.executeQuery();

            rs.last();
            int total_rows = rs.getRow();
            rs.first();rs.previous();

            if(total_rows == 0){
                logger.error(String.format("There's no data for exam ({})", examId));
                return null;
            }
            if(total_rows % subject_num != 0){
                logger.error(String.format("Inequal subjects' num found!"));
                return null;
            }
            
            // Assuming that the result is of the number of N(students)xM(subjects)
            int num_student = total_rows / subject_num;

            float[][] scores = new float[num_student][subject_num];
            int[] ids = new int[num_student];

            for(int i = 0; rs.next(); i++){
                int sub_idx = i % subject_num;
                int student_idx = i / subject_num;

                scores[student_idx][sub_idx] = rs.getFloat("score");
                if(i%subject_num == 0)
                    ids[student_idx] = rs.getInt("studentid");
            }
            
            return new ImmutablePair<>(ids, scores);
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ImmutablePair<int[], float[][]> getScoresByStudent(int studentId){
        String studentState =
            "select examid, score from AS_SCORE_SUB join AS_STUDENT on AS_STUDENT.STUDENTACCOUNT=AS_SCORE_SUB.STUDENTACCOUNT where studentid=? order by examid, subjectid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(studentState, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, studentId);
            rs = ptmt.executeQuery();

            int subject_num = getSubjectsByStudent(studentId).length;
            if(subject_num == 0){
                logger.error(String.format("There's no subject data for student ({})", studentId));
                return null;
            }

            rs.last();
            int total_rows = rs.getRow();
            rs.first();rs.previous();

            if(total_rows == 0){
                logger.error(String.format("There's no data for exam ({})", studentId));
                return null;
            }
            if(total_rows % subject_num != 0){
                logger.error(String.format("Inequal subjects' num found!"));
                return null;
            }
            
            // Assuming that the result is of the number of N(students)xM(subjects)
            int num_exam = total_rows / subject_num;

            float[][] scores = new float[num_exam][subject_num];
            int[] ids = new int[num_exam];

            for(int i = 0; rs.next(); i++){
                int sub_idx = i % subject_num;
                int student_idx = i / subject_num;

                scores[student_idx][sub_idx] = rs.getFloat("score");
                if(i%subject_num == 0)
                    ids[student_idx] = rs.getInt("examid");
            }
            
            return new ImmutablePair<>(ids, scores);
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public float[][] getNormalizedScoresByStudent(int studentId){
        String sql = 
            "select examid, studentstandard from AS_SCORE_SUB join AS_STUDENT on AS_SCORE_SUB.STUDENTACCOUNT=AS_STUDENT.STUDENTACCOUNT where studentid=? order by examid,subjectid";
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, studentId);
            rs = ptmt.executeQuery();

            int subject_num = getSubjectsByStudent(studentId).length;
            if(subject_num == 0){
                logger.error(String.format("There's no subject data for student ({})", studentId));
                return null;
            }

            rs.last();
            int total_rows = rs.getRow();
            rs.first();rs.previous();

            if(total_rows == 0){
                logger.error(String.format("There's no data for exam ({})", studentId));
                return null;
            }
            if(total_rows % subject_num != 0){
                logger.error(String.format("Inequal subjects' num found!"));
                return null;
            }

            // Assuming that the result is of the number of N(students)xM(subjects)
            int num_exam = total_rows / subject_num;

            float[][] scores = new float[num_exam][subject_num];
            for(int i = 0; rs.next(); i++){
                int sub_idx = i % subject_num;
                int exam_idx = i / subject_num;

                scores[exam_idx][sub_idx] = rs.getFloat("studentstandard");
            }
            return scores;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public ImmutablePair<int[], float[][]> getCombinedLevelsByExam(int examId){
        String sql = "select distinct "+
            "as_comblevel.studentid,AS_COMBLEVEL.COMBID,clevel "+
            "from as_comblevel "+
            "join AS_STUDENT "+
            "join AS_SCORE_SUB "+
            "on AS_STUDENT.STUDENTACCOUNT = AS_SCORE_SUB.STUDENTACCOUNT "+
            "on AS_STUDENT.STUDENTID = AS_COMBLEVEL.STUDENTID "+
            "where AS_SCORE_SUB.EXAMID = ? "+
            "order by as_comblevel.studentid, AS_COMBLEVEL.COMBID";
        
        PreparedStatement ptmt = null; ResultSet rs = null;
        try{
            int[] comb_ids = getAllCombinations().left;
            int comb_num = comb_ids.length;

            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            ptmt.setInt(1, examId);
            rs = ptmt.executeQuery();

            rs.last();
            int total_rows = rs.getRow();
            rs.first();rs.previous();

            if(total_rows == 0){
                logger.error(String.format("There's no data for exam ({})", examId));
                return null;
            }

            if(total_rows % comb_num != 0){
                logger.error(String.format("Inequal subjects' num found!"));
                return null;
            }

            // Assuming that the result is of the number of N(students)xM(subjects)
            int student_num = total_rows / comb_num;

            float[][] levels = new float[student_num][comb_num];
            for(int i = 0; rs.next(); i++){
                int c_idx = i % comb_num;
                int s_idx = i / comb_num;

                levels[s_idx][c_idx] = rs.getFloat("clevel");
            }
            return new ImmutablePair<>(comb_ids, levels);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveNormalizedScores(int examId, ImmutablePair<int[], float[][]> scores){
        String sql =
            "update AS_SCORE_SUB set STUDENTSTANDARD = ? where STUDENTACCOUNT in (select distinct STUDENTACCOUNT from AS_STUDENT where STUDENTID=?) and subjectid=? and examid=?";
        float [][] normalized = scores.right;
        int[] ids = scores.left;
        int[] subjects = getSubjectsByExam(examId);

        if(subjects == null)
            return ;

        PreparedStatement ptmt = null; ResultSet rs = null;
        try {
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < normalized.length; i++){
                float[] row = normalized[i];
                for(int j = 0; j <row.length; j++){
                    ptmt.setFloat(1, normalized[i][j]);
                    ptmt.setInt(2, ids[i]);
                    ptmt.setInt(3, subjects[j]);
                    ptmt.setInt(4, examId);
                    ptmt.addBatch();
                }
            }
            ptmt.executeBatch();
		} catch (SQLException e) {
            e.printStackTrace();
		}finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveRelativeLevels(int studentId, float[] levels){
        final String sql1 =
            "delete from as_sublevel where studentid=%d and subjectid=%d";
        final String sql2 =
            "insert into as_sublevel (studentid, subjectid, rlevel) values (%d, %d, %f)";
        
        int[] subjects = getSubjectsByStudent(studentId);

        if(subjects.length != levels.length)
            throw new InvalidParameterException("Length of levels are not equal to number of subjects.");
        
        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < subjects.length; i++){
                String s = String.format(
                    sql1,
                    studentId,
                    subjects[i]
                    );
                stmt.execute(s);
                s = String.format(
                    sql2,
                    studentId,
                    subjects[i],
                    levels[i]
                );
                stmt.execute(s);
            }
		} catch (SQLException e) {
            e.printStackTrace();
		}finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveCombinationLevels(int studentId, float[] levels, int[] combIds){
        final String sql1 =
            "delete from as_comblevel where studentid=%d and combid=%d";
        final String sql2 =
            "insert into as_comblevel (studentid, combid, clevel) values (%d, %d, %f)";
        
        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < combIds.length; i++){
                String s = String.format(
                    sql1,
                    studentId,
                    combIds[i]
                    );
                stmt.execute(s);
                s = String.format(
                    sql2,
                    studentId,
                    combIds[i],
                    levels[i]
                );
                stmt.execute(s);
            }
		} catch (SQLException e) {
            e.printStackTrace();
		}finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveDifficulties(int examId, int[] ids, float[] diffs){
        final String sql = 
            "update as_examsubject set paperdifficulty=? where examid=? and subjectid=?";
        PreparedStatement ptmt = null;
        try{
            ptmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < ids.length; i++){
                ptmt.setInt(2, examId);
                ptmt.setInt(3, ids[i]);
                ptmt.setFloat(1, diffs[i]);
                ptmt.addBatch();
            }

            ptmt.executeBatch();
        }catch (SQLException e) {
            e.printStackTrace();
		}finally {
            if (ptmt != null) {
                try {
                    ptmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveStandardDeviation(int studentId, float[] stddev){
        final String sql1 = 
            "delete from as_stddev where studentid=%d and subjectid=%d";
        final String sql2 = 
            "insert into as_stddev (studentid, subjectid, stddev) values (%d, %d, %f)";
        int[] subjects = getSubjectsByStudent(studentId);
        
        if(subjects.length != stddev.length)
            throw new InvalidParameterException("Length of stddev are not equal to number of subjects.");

        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < subjects.length; i++){
                String s = String.format(
                    sql1,
                    studentId,
                    subjects[i]
                    );
                stmt.execute(s);
                s = String.format(
                    sql2,
                    studentId,
                    subjects[i],
                    stddev[i]
                );
                stmt.execute(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveSubjectSteadiness(int examId, int[] students, float[][] SS){
        final String sql1 = "delete from as_substeady where studentid=%d and subjectid=%d";
        final String sql2 = "insert into as_substeady (studentid, subjectid, ssteady) values (%d, %d, %f)";
        int[] subjects = getSubjectsByExam(examId);

        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < students.length; i++){
                for(int j = 0; j < subjects.length; j++){
                    String s = String.format(
                        sql1,
                        students[i],
                        subjects[j]
                        );
                    stmt.addBatch(s);;
                    s = String.format(
                        sql2,
                        students[i],
                        subjects[j],
                        SS[i][j]
                    );
                    stmt.addBatch(s);
                    stmt.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveCombinedSteadiness(int examId, int[] students, int[] combs, float[][] CS){
        final String sql1 = "delete from as_combsteady where studentid=%d and combid=%d";
        final String sql2 = "insert into as_combsteady (studentid, combid, csteady) values (%d, %d, %f)";

        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < students.length; i++){
                for(int j = 0; j < combs.length; j++){
                    String s = String.format(
                        sql1,
                        students[i],
                        combs[j]
                        );
                    stmt.addBatch(s);
                    s = String.format(
                        sql2,
                        students[i],
                        combs[j],
                        CS[i][j]
                    );
                    stmt.addBatch(s);
                    stmt.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveRegressionParams(int examId, int studentId, float[][] params){
        final String sql1 = "delete from as_trending where studentid=%d and subjectid=%d";
        final String sql2 = "insert into as_trending (studentid, subjectid, slope, intercept) VALUES (%d, %d, %f, %f)";

        int[] subjects = getSubjectsByExam(examId);
        if(subjects == null)
            return;
        
        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < subjects.length; i++){
                String s = String.format(
                    sql1,
                    studentId,
                    subjects[i]
                    );
                stmt.addBatch(s);
                s = String.format(
                    sql2,
                    studentId,
                    subjects[i],
                    params[i][0],
                    params[i][1]
                );
                stmt.addBatch(s);
                stmt.executeBatch();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void saveCombinedSelection(
        int examId, 
        int[] combIds, 
        int[] priors, 
        int[][] zorder){
        
        final String sql1 = "delete from AS_COMBREPORT where studentid=%d and combid=%d";
        final String sql2 = "insert into AS_COMBREPORT (studentid, combid, maybeselect, zorder) VALUES (%d, %d, %d, %d)";

        int[] students = getStudentIdsByExam(examId);
        if(students == null){
            logger.error(String.format("Error getting students for exam %d", examId));
            return;
        }
        if(students.length != priors.length){
            logger.error(String.format("Student count of exam %d has been changed.", examId));
            return;
        }
        
        Statement stmt = null;
        try {
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

            for(int i = 0; i < students.length; i++){
                for(int j = 0; j < combIds.length; j++){
                    String s = String.format(
                        sql1,
                        students[i],
                        combIds[j]
                        );
                    stmt.addBatch(s);
                    s = String.format(
                        sql2,
                        students[i],
                        combIds[j],
                        priors[i] == j ? 1 : 0, // whether prior combination
                        zorder[i][j]+1
                    );
                    stmt.addBatch(s);
                    stmt.executeBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
