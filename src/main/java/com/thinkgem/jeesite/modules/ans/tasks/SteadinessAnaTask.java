package com.thinkgem.jeesite.modules.ans.tasks;

import org.springframework.stereotype.Service;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.scheduling.annotation.Async;

import java.sql.Connection;
import com.thinkgem.jeesite.modules.ans.*;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * SteadinessAnaTask
 */
@Service
public class SteadinessAnaTask {
    private DbUtils db_util;
    private Logger logger;
    public SteadinessAnaTask(Connection conn){
        db_util = new DbUtils(conn);
        logger = LoggerFactory.getLogger(getClass());
    }

    public DbUtils getDbUtils(){
        return db_util;
    }

    @Async
    public float[][]
    ApplyStandardDeviation(int examId){
        long[] costs = new long[]{0, 0, 0};
        long start = System.currentTimeMillis(), end = 0;

        int[] students = db_util.getStudentIdsByExam(examId);
        end = System.currentTimeMillis();
        costs[0] += end - start;
        
        float[][] stddev = new float[students.length][];

        for(int i = 0; i < students.length; i++){
            start = System.currentTimeMillis();
            ImmutablePair<int[], float[][]> S = 
                db_util.getScoresByStudent(students[i]);
            end = System.currentTimeMillis();
            costs[0] += end - start;

            start = end;
            float[] std = SteadinessAnalyser.getScoresStd(S.right);
            end = System.currentTimeMillis();
            costs[1] += end - start;

            start = end;
            db_util.saveStandardDeviation(students[i], std);
            end = System.currentTimeMillis();
            costs[2] += end - start;
            stddev[i] = std;
        }

        logger.info("[Analying][StandardDeviation]Query costs {}ms.", costs[0]);
        logger.info("[Analying][StandardDeviation]Computing costs {}ms.", costs[1]);
        logger.info("[Analying][StandardDeviation]Saving costs {}ms.", costs[2]);

        return stddev;
    }

    @Async
    public float[][]
    ApplySubjectSteadiness(int examId, float[][] stddevs){
        // int[] students = db_util.getStudentIdsByExam(examId);
        float[][] SS = SteadinessAnalyser.getSubjectSteadinessP(stddevs);

        //db_util.saveSubjectSteadiness(examId, students, SS);

        return SS;
    }

    @Async
    public float[][] ApplySubjectSteadiness2(int examId, float[][] stddevs){
        long start = System.currentTimeMillis(), end = 0;

        int[] students = db_util.getStudentIdsByExam(examId);
        end = System.currentTimeMillis();
        logger.info("[Analysing][SubjectSteadiness]Query costs {}ms", end-start);

        start = end;
        float[][] SS = SteadinessAnalyser.getSubjectSteadiness2(stddevs);
        end = System.currentTimeMillis();
        logger.info("[Analysing][SubjectSteadiness]Computing costs {}ms", end-start);

        start = end;
        db_util.saveSubjectSteadiness(examId, students, SS);
        end = System.currentTimeMillis();
        logger.info("[Analysing][SubjectSteadiness]Saving costs {}ms", end-start);

        return SS;
    }

    @Async
    public float[][]
    ApplyCombinedSteadiness(int examId, float[][] SS){
        long start = System.currentTimeMillis(), end = 0;

        int[] students = db_util.getStudentIdsByExam(examId);
        ImmutablePair<int[], int[][]> C = db_util.getAllCombinations();
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSteadiness]Query costs {}ms", end-start);

        int[] comb_ids = C.left;
        int[][] combinations = C.right;

        start = end;
        float[][] CS = SteadinessAnalyser.getCombinedSteadinessP(SS, combinations);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSteadiness]Computing costs {}ms", end-start);

        start = end;
        db_util.saveCombinedSteadiness(examId, students, comb_ids, CS);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSteadiness]Saving costs {}ms", end-start);

        return CS;
    }

    @Async
    public void ApplyTrendingAnalysis(int examId){
        long[] costs = new long[]{0, 0, 0};
        long start = System.currentTimeMillis(), end = 0;

        int[] students = db_util.getStudentIdsByExam(examId);
        end = System.currentTimeMillis();
        costs[0] += end - start;

        for(int i = 0; i < students.length; i++){
            start = System.currentTimeMillis();
            int student_id = students[i];
            float[][] scores = db_util.getNormalizedScoresByStudent(student_id);
            end = System.currentTimeMillis();
            costs[0] += end - start;

            start = end;
            float[][] params = NumericUtils.LinearRegression(scores);
            end = System.currentTimeMillis();
            costs[1] += end - start;

            start = end;
            db_util.saveRegressionParams(examId, student_id, params);
            end = System.currentTimeMillis();
            costs[2] += end - start;
        }

        logger.info("[Analying][Trending]Query costs {}ms.", costs[0]);
        logger.info("[Analying][Trending]Computing costs {}ms.", costs[1]);
        logger.info("[Analying][Trending]Saving costs {}ms.", costs[2]);
    }
}
