package com.thinkgem.jeesite.modules.ans.tasks;

import java.sql.Connection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.thinkgem.jeesite.modules.ans.DbUtils;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * AllInTask
 */
@Service
public class AllInTask {
    private LevelAnalysingTask ltask;
    private AssignmentAnaTask atask;
    private SteadinessAnaTask stask;
    private DbUtils db_util;
    public AllInTask(Connection conn){
        db_util = new DbUtils(conn);
        ltask = new LevelAnalysingTask(conn);
        atask = new AssignmentAnaTask(conn);
        stask = new SteadinessAnaTask(conn);
    }

    public DbUtils getDbUtils(){
        return db_util;
    }

    @Async
    public CompletableFuture<Void> ApplyAll(int examId){
        // Computing levels
        atask.ApplyDifficulty(examId)
            .thenRun(() -> ltask.ApplyNormalizeScoreOfGrade(examId))
            .join();

        int[] students = db_util.getStudentIdsByExam(examId);
        CompletableFuture[] tasks = new CompletableFuture[students.length];

        for(int i = 0; i < students.length; i++){
            int s = students[i];
            CompletableFuture f = 
                ltask.ApplyRelativeLevel(s)
                .thenRun(()-> ltask.ApplyCombinedLevel(s));
            tasks[i] = f;
        }

        CompletableFuture ltasks = CompletableFuture.allOf(tasks);
        CompletableFuture cstask = ltasks.thenRunAsync(
            ()-> ltask.ApplyCombinedSelection(examId));

        // Computing standard deviation
        CompletableFuture<float[][]> stddev_task = 
            CompletableFuture.supplyAsync(()-> stask.ApplyStandardDeviation(examId));
            
        // Computing combined steadiness
        CompletableFuture<Void> comsteady_task = stddev_task
            .thenApply(s -> stask.ApplySubjectSteadiness(examId, s))
            .thenAccept(ss -> stask.ApplyCombinedSteadiness(examId, ss));
        
        //Computing trending line parameters
        CompletableFuture<Void> reg_task = CompletableFuture.runAsync(
            () -> stask.ApplyTrendingAnalysis(examId));
        
        // Computing new version of subject steadiness.
        float[][] dev = stddev_task.join();
        CompletableFuture<Void> ssteady_task = CompletableFuture.runAsync(
            () -> stask.ApplySubjectSteadiness2(examId, dev));

        return CompletableFuture.allOf(cstask, comsteady_task, reg_task, ssteady_task);
    }
}