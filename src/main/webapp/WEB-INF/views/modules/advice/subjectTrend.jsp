<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>科目成绩走向</title>
<meta name="decorator" content="default"/>
<script src="${ctxStatic}/echart/echarts.js" type="text/javascript"></script>
<style>
	@media screen and (max-width:1280px) {
		#yuwen,#shuxue,#waiyu,#wuli,#huaxue,#shengwu,#lishi,#zhengzhi,#dili{ overflow-x:auto;}
	}
	@media screen and (min-width:1290px) {
		#yuwen,#shuxue,#waiyu,#wuli,#huaxue,#shengwu,#lishi,#zhengzhi,#dili{ overflow:hidden}
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
	var url = "echart5";
	$.ajax({
		url:url,
		dataType:"json",
		type:"POST",
		success:function(data){
			console.log(data);
			$("#count").html(data.count+"           ")
			$("#subject").html(data.best.substring(0,data.best.length-1)+"           ");
			$.each(data,function(key,value){
				if(key == '语文' ){
					createEchart("yuwen",data.scoreTime,value.score,key,value.funl,value.start,value.end);
				}
                if(key == '数学' ){
                    createEchart("shuxue",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '外语' ){
                    createEchart("waiyu",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '物理' ){
                    createEchart("wuli",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '化学' ){
                    createEchart("huaxue",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '生物' ){
                    createEchart("shengwu",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '历史' ){
                    createEchart("lishi",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '地理' ){
                    createEchart("dili",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
                if(key == '政治' ){
                    createEchart("zhengzhi",data.scoreTime,value.score,key,value.funl,value.start,value.end);
                }
			});
		},
		error:function(){
			
		}
	})
})

function createEchart(id,x,y,title,zhi,start,end){
	var data = [];
	var startPoint = [0,start];
	var endPoint = [x[x.length-1],end];
	for(var i =0;i<x.length;i++){
		var xx = x[i];
		var yy = y[i];
		data[i] = [xx,yy];

	}
	var dom = document.getElementById(id);
	var myChart = echarts.init(dom);
	option = {
		    title: {
		        text: title,
		        x: 'center',
		        y: 0
		    },
		    xAxis:  {
                type:'category',
                name:'历次考试',
                nameTextStyle:{
                	fontSize:10
                },
                data:x,
                interval: 0,
                axisLabel:{
                    show:true
                },
                axisLine:{
                	onZero:false
                }
            },
		    yAxis: {name:'标准分', min: -5,max:5,grid:0,interval:1 },
		    series: [
		        {
		            name: title,
		            type: 'scatter',
		            data: data,
		            markLine: {
		                animation: false,
		                label: {
		                    normal: {
		                        formatter: zhi,
		                        textStyle: {
		                            align: 'right'
		                        }
		                    }
		                },
		                lineStyle: {
		                    normal: {
		                        type: 'solid'
		                    }
		                },
		                tooltip: {
		                    formatter: zhi
		                },
		                data: [[{
		                    coord: startPoint,
		                    symbol: 'none'
		                }, {
		                    coord: endPoint,
		                    symbol: 'none'
		                }]]
		            }
		        }
		    ]
		};
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/adviceStu/controller/subTrend">科目成绩走向</a></li>
	</ul>
	<div  style = "width:100%;float:left;position:relative;left:65%;">年级学生数：<span id = "count"></span>推荐科目：<span id = "subject"></span>
	</div>
	<div id="contentChart" style="text-align:center;width:100%" >
		<div style = 'float:left;width:45%;height:400px;' id = 'yuwen'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'shuxue'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'waiyu'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'wuli'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'huaxue'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'shengwu'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'lishi'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'dili'></div>
		<div style = 'float:left;width:45%;height:400px;' id = 'zhengzhi'></div>
	</div>
</body>
</html>