<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>总体特征分析</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teacher/analysisOfAll/analysis">总体特征分析</a></li>
	</ul><br/>
	<form id="searchForm" modelAttribute="analysis" class="form-horizontal breadcrumb form-search ">
		<ul class="ul-form">
			<li>
 				<select id="firstYear" name="firstYear" class="input-medium" onchange="firstChange(this.options[this.options.selectedIndex].value)">
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
					<option value="2023">2023</option>
					<option value="2024">2024</option>
					<option value="2025">2025</option>
				</select>
				<select name="termCode" id = "termName" onchange="termChange(this.options[this.options.selectedIndex].value)">
					<c:forEach items ="${termList}" var = "term">
						<option value="${term.termCode}" >${term.termName}</option>
					</c:forEach>
				<select>
			</li>
			<script type="text/javascript"> 
				function firstChange(v){
					$.ajax({
				        type : "post",
				        async : false,
				        url : "getExamList",
				        data : {
				            'first' : v
				        },
				        dataType : "json",
				        success : function(msg) {
				        $("#examName").empty();
				            if (msg.length > 0) {
				                for (var i = 0; i < msg.length; i++) {
				                        var examId = msg[i].examId;
				                        var examName = msg[i].examName;
				                        var $option = $("<option>").attr({
				                            "value" : examId
				                        }).text(examName);
				                        $("#examName").append($option);
				                }
				            }
				            $("#examName").change();
				        },
				        error : function(json) {
				            $.jBox.alert("网络异常！");
				        }
				    });
				}
				function termChange(v){
					$.ajax({
				        type : "post",
				        async : false,
				        url : "getSubjectList",
				        data : {
				            'termCode' : v
				        },
				        dataType : "json",
				        success : function(msg) {
				            $("#subjectName").empty();
				            if (msg.length > 0) {
				                for (var i = 0; i < msg.length; i++) {
				                        var subjectId = msg[i].subjectId;
				                        var subjectName = msg[i].subjectName;
				                        var $option = $("<option>").attr({
				                            "value" : subjectId
				                        }).text(subjectName);
				                        $("#subjectName").append($option);
				                }
				            }
				            $("#subjectName").change();
				        },
				        error : function(json) {
				            $.jBox.alert("网络异常！");
				        }
				    });
				}
			</script>
			<li>
				<label>考试：</label>
				<select name="examId" id = "examName">
					<c:forEach items ="${examList}" var = "exam">
						<option value="${exam.examId}" >${exam.examName}</option>
					</c:forEach>
				<select>
			</li>
			<li>
				<label>科目：</label>
				<select name="subjectId" id = "subjectName">
					<c:forEach items ="${subjectList}" var = "subject">
						<option value="${subject.subjectId}" >${subject.subjectName}</option>
					</c:forEach>
					<option value="allScore" >总成绩</option>
				<select>
			</li>
		</ul>
	</form>
	<div>
		<ul class="ul-form">
			年级平均分：<span id = "gradeStandardScore"></span>
			标准差：<span id = "gradeStandardDeviation"></span>
			及格率：<span id = "gradeStandardDeviation"></span>
			优秀率：<span id = "gradeStandardDeviation"></span>
		</ul>
	</div>
	<div>	
		<h5>成绩分布曲线图</h5>
		<div style = "width:70%;height:100px;" id = "classChart">
			
		</div>
	</div>
	<div>
		<c:forEach items ="${classList}" var = "class">
			<a >${class.className}</a>
		</c:forEach>
	 </div>
	<div>
		<ul class="ul-form">
			班级平均分：<span id = "classStandardScore"></span>
			标准差：<span id = "classStandardDeviation"></span>
			及格率：<span id = "classStandardDeviation"></span>
			优秀率：<span id = "classStandardDeviation"></span>
		</ul>
	</div>
	<div>	
		<h5>成绩分布曲线图</h5>
		<div style = "width:70%;height:100px;" id = "classChart">
			
		</div>
	</div>
</body>
</html>