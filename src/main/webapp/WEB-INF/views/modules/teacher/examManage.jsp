<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>考试管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var date = new Date;
			var year3 = date.getFullYear();
			var year2 = year3 - 1;
			var year1 = year3 - 2;
			document.getElementById("firstYear").options.add(new Option("",""));
			document.getElementById("firstYear").options.add(new Option(year1,year1));
			document.getElementById("firstYear").options.add(new Option(year2,year2));
			document.getElementById("firstYear").options.add(new Option(year3,year3));
			$("#btnImportSubjectScore").click(function(){
				$.jBox($("#importSubjectScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
			$("#btnImportQuestionScore").click(function(){
				$.jBox($("#importQuestionScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
		});
		function page(n,s){
			if(n) $("#pageNo").val(n);
			if(s) $("#pageSize").val(s);
			$("#searchForm").attr("action","${ctx}/teacher/exam/list");
			$("#searchForm").submit();
	    	return false;
	    }
		function importSubjectScore() {
			$.jBox($("#importSubjectScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
				bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
		}
		function importQuestionScore() {
			$.jBox($("#importQuestionScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
				bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
		}
	</script>
</head>
<body>
	<div id="importSubjectScoreBox" class="hide">
		<form id="importSubjectScoreForm" action="${ctx}/teacher/score/importSubjectScore" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadSubjectScoreFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubjectScoreSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
		</form>
	</div>
	<div id="importQuestionScoreBox" class="hide">
		<form id="importQuestionScoreForm" action="${ctx}/teacher/score/importQuestionScore" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadQuestionScoreFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportQuestionScoreSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teacher/exam/list">考试列表</a></li>
		<shiro:hasPermission name="teacher:exam:edit"><li><a href="${ctx}/teacher/exam/form">考试添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="exam" action="${ctx}/teacher/exam/list" method="post" class="breadcrumb form-search ">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		
 		<ul class="ul-form">
 			<li>
 				<select id="firstYear" name="firstYear" class="input-medium">
				</select>
				<label style="width: auto !important;">级</label>
				<form:select path="termCode" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getTermList()}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li>
				<label>创建时间：</label>
				<input id="beginDate"  name="beginDate"  type="text" readonly="readonly" maxlength="20" class="input-medium Wdate" style="width:163px;"
					value="<fmt:formatDate value="${act.beginDate}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
					　至　
				<input id="endDate" name="endDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate" style="width:163px;"
					value="<fmt:formatDate value="${act.endDate}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询" onclick="return page();"/>
				<a href="${ctx}/teacher/exam/form"><input class="btn btn-primary" type="button" value="新增"/></a>
			</li>
			<li>
				<a href="${ctx}/teacher/score/importSubjectScore/template">&nbsp;&nbsp;下载科目成绩模板&nbsp;&nbsp;</a>
			</li>
			<li>
				<a href="${ctx}/teacher/score/importQuestionScore/template">&nbsp;&nbsp;下载试题成绩模板&nbsp;&nbsp;</a>
			</li>
		</ul>
		
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead></th><th>日期</th><th>名称</th><th>学期</th><shiro:hasPermission name="teacher:exam:edit"><th>操作</th></shiro:hasPermission></tr></thead>
		<tbody>
		<c:forEach items="${page.list}" var="exam">
			<tr>
				<td>${exam.examDate}</td>
				<td>${exam.examName}</td>
				<td>${fns:getTermName(exam.termCode)}</td>
				<shiro:hasPermission name="teacher:exam:edit"><td>
    				<a href="${ctx}/teacher/exam/form?id=${exam.examId}">修改</a>
					<a href="${ctx}/teacher/exam/delete?id=${exam.examId}" onclick="return confirmx('确认要删除此次考试吗？', this.href)">删除</a>
					<a href="javascript:void(0)" onclick="importSubjectScore()">导入科目成绩</a>
					<a href="javascript:void(0)" onclick="importQuestionScore()">导入试题得分</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>