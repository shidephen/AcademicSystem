<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>考试管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#inputForm").validate({
  				rules: {
					fullScore: {digits:true}
				},
				messages: {
					fullScore: {digits:"必须为数字"}
				},  
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if ($("#subjectClass").val() === '') {
						$("#subjectClass").parent().append('<label for="type" class="error">必填信息</label>');
					}
 					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					} 
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teacher/subject/list">科目列表</a></li>
		<li class="active"><a href="${ctx}/teacher/subject/form?id=${subject.subjectId}">科目添加<shiro:lacksPermission name="teacher:subject:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="subject" action="${ctx}/teacher/subject/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<div class="control-group">
			<label class="control-label">科目分类:</label>
			<div class="controls">
				<select id="subjectClass" name="subjectClass" class="input-medium">
					<option value="" selected="selected"></option>
					<option value="1">必考类</option>
					<option value="2">选考类</option>
					<option value="3">其他类</option>
				</select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">科目名称:</label>
			<div class="controls">
				<form:input path="subjectName" htmlEscape="false" maxlength="6" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">科目满分:</label>
			<div class="controls">
				<form:input path="fullScore" htmlEscape="false" maxlength="3" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">校优势学科:</label>
			<div class="controls">
 				<input type="radio" name="isAdvanced" value="1" checked="checked" />是 
				<input type="radio" name="isAdvanced" value="0" />否
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">是否在用:</label>
			<div class="controls">
 				<input type="radio" name="isValid" value="1" checked="checked" />是 
				<input type="radio" name="isValid" value="0" />否
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="teacher:student:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>