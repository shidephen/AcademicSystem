<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学生管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var date = new Date;
			var year3 = date.getFullYear();
			var year2 = year3 - 1;
			var year1 = year3 - 2;
			document.getElementById("firstYear").options.add(new Option("",""));
			document.getElementById("firstYear").options.add(new Option(year1,year1));
			document.getElementById("firstYear").options.add(new Option(year2,year2));
			document.getElementById("firstYear").options.add(new Option(year3,year3));
			$("input[name=all]").click(function () {
		        if (this.checked) {
		            $("tbody :checkbox").prop("checked", true);
		            console.log();
		        } else {
		            $("tbody :checkbox").prop("checked", false);
		        }
		    });
			$("#btnImport").click(function(){
				$.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
 			$("#btnDeleteAll").click(function(){
 				var chk_value =[];
 			    $("tbody :checkbox:checked").each(function(){ 
 			    	chk_value.push(this.value); //push 进数组
 			    });
 			    //数组转字符串
 			    var ids = chk_value.length==0 ? '' : chk_value.join(",");
 				return confirmx('确认要删除选中学生吗？', "${ctx}/teacher/student/deleteAll?ids=" + ids);
			});
			$("#firstYear").change(function(){  
		        var firstYear = $("#firstYear").val();
		        $.ajax({  
		            type:"POST",  
		            url: "${ctx}/teacher/student/getClass",  
		            data: JSON.stringify({"firstYear": firstYear}), 
		            dataType: "json",
		            contentType: "application/json;charset=utf-8",
		            success: function(data){  
		            	document.getElementById("classId").options.length=0;
		                if(data != null && data.length > 0){  
		                	document.getElementById("classId").options.add(new Option("",""));
		                    for(var i=0; i<data.length; i++){ 
		                    	document.getElementById("classId").options.add(new Option(data[i].classCode,data[i].classId));
		                    }
			                $("#classId").attr("disabled",false);
		                }  
		            },error: function (XMLHttpRequest, textStatus, errorThrown) {
	                    // 状态码
	                    console.log(XMLHttpRequest);
	                    // 状态
	                    console.log(textStatus);
	                    // 错误信息   
	                    console.log(errorThrown);
	                }
		        });
		    }); 
		});
		function page(n,s){
			if(n) $("#pageNo").val(n);
			if(s) $("#pageSize").val(s);
			$("#searchForm").attr("action","${ctx}/teacher/student/list");
			$("#searchForm").submit();
	    	return false;
	    }
		function deleteAll() {
			var chk_value =[];
		    $("tbody :checkbox:checked").each(function(){ 
		    	chk_value.push(this.value); //push 进数组
		    });
		    //数组转字符串
		    var ids = chk_value.length==0 ? '' : chk_value.join(",");
		    var myForm = document.createElement("form");
		    myForm.method = "post";
		    myForm.action = "${ctx}/teacher/student/deleteAll?ids=" + ids;
		    document.body.appendChild(myForm);
		    myForm.submit();
/* 		    $.ajax({  
	            type:"POST",  
	            url: "${ctx}/teacher/student/deleteAll?ids=" + ids, 
	            dataType: "json",
	            contentType: "application/json;charset=utf-8",
	            success: function(data){  
	            	top.$('.jbox-body .jbox-icon').css('top','55px');
	            },error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // 状态码
                    console.log(XMLHttpRequest);
                    // 状态
                    console.log(textStatus);
                    // 错误信息   
                    console.log(errorThrown);
                }
	        }); */
		}
	</script>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teacher/student/import" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<%-- <a href="${ctx}/teacher/student/import/template">下载模板</a> --%>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teacher/student/list">学生列表</a></li>
		<shiro:hasPermission name="teacher:student:edit"><li style="display:none;"><a href="${ctx}/teacher/student/form">学生添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="student" action="${ctx}/teacher/student/list" method="post" class="breadcrumb form-search ">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		
 		<ul class="ul-form">
			<li>
				<label>年级：</label>
 				<select id="firstYear" name="firstYear" class="input-medium">
					<%-- <option value="" selected="selected"></option>
					<option value=year1>${year1}</option>
					<option value=year2>${year2}</option>
					<option value=year3>${year3}</option> --%>
				</select>
			</li>
			<li>
				<label>班级：</label>
				<select id="classId" name="classId" class="input-medium" disabled="disabled">
				</select>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询" onclick="return page();"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				<input id="btnDeleteAll" class="btn btn-primary" type="button" value="批量删除" />
			</li>
			<li>
				<a href="${ctx}/teacher/student/import/template">&nbsp;&nbsp;下载学生信息模板&nbsp;&nbsp;</a>
			</li>
			<li class="clearfix"></li>
		</ul>
		
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead><tr><th><input type="checkbox" name="all"></th><th>账号</th><th class="sort-column">入学年份</th><th>班级</th><th class="sort-column">学号</th><th class="sort-column name">姓名</th><th>性别</th><shiro:hasPermission name="teacher:student:edit"><th>操作</th></shiro:hasPermission></tr></thead>
		<tbody>
		<c:forEach items="${page.list}" var="student">
			<tr>
				<td><input type="checkbox" name="checkboxes" style="text-align: center" value='${student.studentId}'></td>
				<td>${student.studentAccount}</td>
				<td>${student.firstYear}</td>
				<td>${student.classId}</td>
				<td>${student.studentNo}</td>
				<td>${student.name}</td>
				<td>${fns:getGender(student.sex)}</td>
				<shiro:hasPermission name="teacher:student:edit"><td>
    				<a href="${ctx}/teacher/student/form?id=${student.studentId}">修改</a>
					<a href="${ctx}/teacher/student/delete?id=${student.studentId}" onclick="return confirmx('确认要删除该学生吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>