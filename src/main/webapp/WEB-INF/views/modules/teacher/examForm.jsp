<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>考试管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#inputForm").validate({
/* 				rules: {
					loginName: {remote: "${ctx}/sys/user/checkLoginName?oldLoginName=" + encodeURIComponent('${user.loginName}')}
				},
				messages: {
					loginName: {remote: "用户登录名已存在"},
					confirmNewPassword: {equalTo: "输入与上面相同的密码"}
				}, */
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teacher/exam/list">考试列表</a></li>
		<li class="active"><a href="${ctx}/teacher/exam/form?id=${exam.examId}">考试<shiro:hasPermission name="teacher:exam:edit">${not empty exam.examId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teacher:exam:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="exam" action="${ctx}/teacher/exam/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<div class="control-group">
			<label class="control-label">考试日期:</label>
			<div class="controls">
				<input id="examDate" name="examDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" style="width:163px;"
					value="<fmt:formatDate value="${exam.examDateDate}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span id="examDateSpan" class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">年级:</label>
			<div class="controls">
				<select id="firstYear" name="firstYear" class="input-medium required" value="${exam.firstYear}">
					<option value="" selected="selected"></option>
					<option value="2016" <c:if test="${exam.firstYear=='2016'}">selected</c:if> >2016</option>
					<option value="2017" <c:if test="${exam.firstYear=='2017'}">selected</c:if> >2017</option>
					<option value="2018" <c:if test="${exam.firstYear=='2018'}">selected</c:if> >2018</option>
					<option value="2019" <c:if test="${exam.firstYear=='2019'}">selected</c:if> >2019</option>
					<option value="2020" <c:if test="${exam.firstYear=='2020'}">selected</c:if> >2020</option>
					<option value="2021" <c:if test="${exam.firstYear=='2021'}">selected</c:if> >2021</option>
					<option value="2022" <c:if test="${exam.firstYear=='2022'}">selected</c:if> >2022</option>
					<option value="2023" <c:if test="${exam.firstYear=='2023'}">selected</c:if> >2023</option>
					<option value="2024" <c:if test="${exam.firstYear=='2024'}">selected</c:if> >2024</option>
					<option value="2025" <c:if test="${exam.firstYear=='2025'}">selected</c:if> >2025</option>
				</select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">学期:</label>
			<div class="controls">
				<form:select path="termCode" class="input-medium required">
					<form:option value="" label=""/>
					<form:options items="${fns:getTermList()}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">考试名称:</label>
			<div class="controls">
				<form:input path="examName" htmlEscape="false" maxlength="33" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">考试科目:</label>
			<div class="controls">
				<c:if test="${exam.examId!=null}">
					<form:checkboxes path="subjectList" items="${fns:getSubjectList()}" itemLabel="label" itemValue="value" htmlEscape="false" class="required" />
				</c:if>
				<c:if test="${exam.examId==null}">
					<form:checkboxes path="subjectList" items="${fns:getSubjectList()}" itemLabel="label" itemValue="value" htmlEscape="false" checked="checked" class="required" />
				</c:if>
				<span id="subjectIdSpan" class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="teacher:student:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>