<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>成绩管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var date = new Date;
			var year3 = date.getFullYear();
			var year2 = year3 - 1;
			var year1 = year3 - 2;
			document.getElementById("firstYear").options.add(new Option("",""));
			document.getElementById("firstYear").options.add(new Option(year1,year1));
			document.getElementById("firstYear").options.add(new Option(year2,year2));
			document.getElementById("firstYear").options.add(new Option(year3,year3));
			$("#btnImportSubjectScore").click(function(){
				$.jBox($("#importSubjectScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
			$("#btnImportQuestionScore").click(function(){
				$.jBox($("#importQuestionScoreBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
			$("#firstYear").change(function(){  
		        var firstYear = $("#firstYear").val();
		        $.ajax({  
		            type:"POST",  
		            url: "${ctx}/teacher/student/getClass",  
		            data: JSON.stringify({"firstYear": firstYear}), 
		            dataType: "json",
		            contentType: "application/json;charset=utf-8",
		            success: function(data){  
		            	document.getElementById("classId").options.length=0;
                    	document.getElementById("classId").options.add(new Option("", ""));
		                if(data != null && data.length > 0){  
		                    for(var i=0; i<data.length; i++){ 
		                    	document.getElementById("classId").options.add(new Option(data[i].classCode,data[i].classId));
		                    }  
		                }
		                $("#classId").attr("disabled",false);
		                $("#termCode").attr("disabled",false);
		            },error: function (XMLHttpRequest, textStatus, errorThrown) {
	                    // 状态码
	                    console.log(XMLHttpRequest);
	                    // 状态
	                    console.log(textStatus);
	                    // 错误信息   
	                    console.log(errorThrown);
	                }
		        })  
		    });
			$("#termCode").change(function(){  
		        var firstYear = $("#firstYear").val();
		        var termCode = $("#termCode").val();
		        $.ajax({  
		            type:"POST",  
		            url: "${ctx}/teacher/score/getExamName",  
		            data: JSON.stringify({"firstYear": firstYear,"termCode":termCode}), 
		            dataType: "json",
		            contentType: "application/json;charset=utf-8",
		            success: function(data){  
		            	document.getElementById("examId").options.length=0;
		                if(data != null && data.length > 0){  
		                    for(var i=0; i<data.length; i++){ 
		                    	document.getElementById("classId").options.add(new Option(data[i].examName,data[i].examId));
		                    }  
		                }
		            },error: function (XMLHttpRequest, textStatus, errorThrown) {
	                    // 状态码
	                    console.log(XMLHttpRequest);
	                    // 状态
	                    console.log(textStatus);
	                    // 错误信息   
	                    console.log(errorThrown);
	                }
		        })  
		    });
		});
		function page(n,s){
			if(n) $("#pageNo").val(n);
			if(s) $("#pageSize").val(s);
			$("#searchForm").attr("action","${ctx}/teacher/score/list");
			$("#searchForm").submit();
	    	return false;
	    }
	</script>
</head>
<body>
	<div id="importSubjectScoreBox" class="hide">
		<form id="importSubjectScoreForm" action="${ctx}/teacher/score/importSubjectScore" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadSubjectScoreFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubjectScoreSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teacher/score/importSubjectScore/template">下载科目成绩模板</a>
		</form>
	</div>
	<div id="importQuestionScoreBox" class="hide">
		<form id="importQuestionScoreForm" action="${ctx}/teacher/score/importQuestionScore" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadQuestionScoreFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportQuestionScoreSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teacher/score/importQuestionScore/template">下载试题成绩模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teacher/score/list">成绩列表</a></li>
		<shiro:hasPermission name="teacher:score:edit"><li style="display:none;"><a href="${ctx}/teacher/score/form">学生各科成绩</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="score" action="${ctx}/teacher/score/list" method="post" class="breadcrumb form-search ">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		
 		<ul class="ul-form">
			<li>
				<label>年级：</label>
 				<select id="firstYear" name="firstYear" class="input-medium">
				</select>
			</li>
			<%-- <li>
				<form:select path="termCode" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getTermList()}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li> --%>
			<li>
				<label>班级：</label>
				<select id="classId" name="classId" class="input-medium" disabled="disabled">
				</select>
			</li>
			<li>
				<label>考试名称：</label>
				<input id="examName" name="examName" class="input-medium"/>
				<!-- <select id="examId" name="examId" class="input-medium" disabled="disabled">
				</select> -->
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询" onclick="return page();"/>
				<!-- <input id="btnImportSubjectScore" class="btn btn-primary" type="button" value="导入科目成绩"/>
				<input id="btnImportQuestionScore" class="btn btn-primary" type="button" value="导入试题得分"/> -->
			</li>
			<li class="clearfix"></li>
		</ul>
		
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead><tr><th>考试名称</th><th>班级</th><th>学生姓名</th><th>总成绩</th><th>语文</th><th>数学</th><th>外语</th><th>物理</th><th>化学</th><th>生物</th><th>政治</th><th>历史</th><th>地理</th><th>技术</th></tr></thead>
		<tbody>
		<c:forEach items="${page.list}" var="score">
			<tr>
				<td>${score.examName}</td>
				<td>${score.className}</td>
				<td>${score.studentName}</td>
				<td>${score.total}</td>
				<td>${score.chinese}</td>
				<td>${score.math}</td>
				<td>${score.english}</td>
				<td>${score.physical}</td>
				<td>${score.chemistry}</td>
				<td>${score.biology}</td>
				<td>${score.ideology}</td>
				<td>${score.history}</td>
				<td>${score.geography}</td>
				<td>${score.technology}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>