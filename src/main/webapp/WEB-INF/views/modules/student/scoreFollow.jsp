<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>成绩追踪</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/echart/echarts.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	search();
	$("#search").on("click",search);
})
function search(){
	var url = "follow";
	$.ajax({
		url:url,
		dataType:"json",
		type:"POST",
		data:{key:$("#subjectName").val()},
		success:function(data){
			createEchart(data.x,data.y);
		},
		error:function(){
		}
	})
}
function createEchart(x,y){
	var dom = document.getElementById("contentChart");
	var myChart = echarts.init(dom);
	option = null;
	option = {
	    xAxis: {
	        type: 'category',
	        data: x,
	        name:'历次考试'
	    },
	    yAxis: {
	        type: 'value',
	        name:'标准分'
	    },
	    series: [{
	        data: y,
	        type: 'line',
	        label:{ 
	            normal:{ 
		            show: true, 
		            position: 'top'
	            } 
	         }
	    }]
	};
	;
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/scoreSearch/scoreFollow">成绩追踪</a></li>
	</ul>
	<div  style = "float:right;position:relative;right:30%;">
		科目：
		<select  id = "subjectName">
			<c:forEach items ="${subjectList}" var = "subject">
				<option value="${subject.subjectId}" >${subject.subjectName}</option>
			</c:forEach>
			<option value="allScore" >总成绩</option>
		<select>
		<input id="search" class="btn btn-primary" type="submit" value="查询" />
	</div>
	<div id="contentChart" style="text-align:center;width:90%;height:500px;" >
		
	</div>
</body>
</html>