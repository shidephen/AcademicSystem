package com.thinkgem.jeesite.test;

import com.thinkgem.jeesite.modules.ans.AnalysingDefinations;
import com.thinkgem.jeesite.modules.ans.SteadinessAnalyser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Assert;
import org.junit.Test;

// Used to test algorithm
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import com.thinkgem.jeesite.modules.ans.LevelAnalyser;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestLevelAnalysing{
    // nxm
    // n is student number
    // m is course number
    public double[][] scoresPerAssign = new double[][]{
        {88, 60, 98, 78, 87, 89},
        {97, 88, 78, 91, 87, 88}
    };

    public float[][] difficulties = new float[][]{
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f},
        {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f}
    };

    public float scores[][] = new float[][]{
            {15,13,26,20,10,14},
            {14,13,24,20,10,11},
            {14,13,26,15,10,14},
            {14,10,24,20,10,13},
            {15,13,26,15,10,10},
            {12,12,24,18,10,12},
            {14,12,20,18,8,14},
            {15,11,26,15,8,10},
            {15,14,20,15,10,10},
            {13,10,24,12,10,14},
            {11,11,24,12,10,14},
            {14,9,26,10,8,14},
            {14,10,26,10,10,14},
            {15,14,24,2,10,14},
            {12,11,14,10,10,12},
            {13,10,23,18,8,6},
            {14,11,20,10,10,12},
            {12,14,22,5,10,14},
            {12,12,18,12,10,12},
            {13,13,20,10,5,14},
            {10,13,18,15,8,10},
            {13,12,18,10,6,14},
            {12,11,26,5,10,8},
            {10,13,16,15,10,8},
            {10,13,14,10,10,14},
            {11,10,20,10,10,10},
            {12,8,18,12,6,14},
            {12,10,16,10,8,14},
            {15,11,26,5,8,5},
            {10,11,16,15,5,12},
            {9,10,18,7,10,14},
            {13,10,20,0,10,13},
            {13,10,20,0,10,13},
            {8,8,16,10,10,14},
            {8,9,26,7,6,9},
            {7,11,10,15,10,10},
            {13,11,19,10,8,2},
            {9,12,16,7,10,8},
            {12,14,18,7,8,2},
            {12,10,16,10,10,2},
            {11,11,18,10,2,5},
            {9,11,10,12,8,2},
            {11,12,10,4,8,5},
            {7,11,12,0,8,10},
            {10,11,10,0,5,5},
            {10,11,10,2,0,0}
    };
    public INDArray Scores = Nd4j.create(scores);

    public int[][] combinations = new int[][]{
            {1, 3, 5},
            {2, 4, 6}
    };

    @Test
    public void TestRelativeLevelAlg(){
        // Input: NxC Normalized scores per assignment
        // N is assignment number
        // C is course number
        INDArray scoresPerAssign = Nd4j.create(this.scoresPerAssign);
        INDArray difficulties = Nd4j.randn(scoresPerAssign.shape());

        INDArray normalized = NormalizeScore(scoresPerAssign, difficulties);

        INDArray mean = normalized.mean(0);

        float[] relativeLevel = mean.data().asFloat().clone();

    }

    public INDArray NormalizeScore(INDArray scores, INDArray difs){
        // Input: NxC
        // N is student number
        // C is course number
        INDArray weightedScores = scores.mul(difs);
        INDArray mean = weightedScores.mean(0).repeat(0, weightedScores.rows());
        INDArray std = weightedScores.std(0).repeat(0, weightedScores.rows());

        INDArray normalized = weightedScores.sub(mean).div(std);

        return normalized;
    }

    @Test
    public void TestCombinationLevelAlg(){
        // Input: Normalized relative scores per student Nx1
        // Returne: 20 combined scores.
        INDArray scoresPerAssign = Nd4j.rand(20, 10);
        ArrayList<Float> C = new ArrayList<Float>();

        for(int i = 0; i < combinations.length; i++) {
            float val = scoresPerAssign
                    .get(new NDArrayIndex(combinations[i]))
                    .meanNumber()
                    .floatValue();
            C.add(val);
        }
    }

    @Test
    public void TestNormalizeTotalLib(){
        INDArray sum = Scores.sum(1);
        float[] exp = new float[]{1.9452565f, 1.5011781f ,1.5011781f, 1.42716503f, 1.27913889f,
                1.20512582f, 1.05709969f, 0.98308662f, 0.90907355f,  0.83506048f,
                0.76104742f, 0.68703435f, 0.90907355f, 0.53900821f, -0.20112247f,
                0.46499514f, 0.39098208f, 0.39098208f, 0.31696901f,  0.24295594f,
                0.16894287f, 0.0949298f , 0.02091674f, 0.02091674f, -0.05309633f,
                -0.05309633f, 0.1271094f , -0.1271094f , -0.1271094f, -0.20112247f,
                -0.27513554f, -0.42316167f, -0.42316167f, -0.42316167f, -0.49717474f,
                -0.64520088f, -0.64520088f, -0.71921394f, -0.79322701f, -0.86724008f,
                -1.08927928f, -1.45934462f, -1.60737076f, -1.75539689f, -2.27348837f,
                -2.86559291f};
        float[] norm = LevelAnalyser.NormalizeScore(sum.data().asFloat());
//        for(int i = 0; i < exp.length; i++)
//            assertEquals(exp[i], norm[i], 0.1f);
    }

    @Test
    public void TestRelativeLevelLib(){
        float[] levels = LevelAnalyser.getRelativeLevel(scores, difficulties);
        assertEquals(6, levels.length);
    }

    @Test
    public void TestCombinationLevelLib(){
        ArrayList<Float> level = LevelAnalyser.getCombinationLevel(scores, combinations);
        assertEquals(2, level.size());
        float[] levels = LevelAnalyser.getCombinationLevel(scores, difficulties, combinations);
        assertEquals(2, levels.length);
    }

    @Test
    public void TestHistogram(){
        float[] edges = new float[]{0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        int[] counts = new int[]{ 0,  0,  0,  1,  3,  3, 14, 12,  9,  4};
        INDArray sum = Scores.sum(1);

        ImmutablePair<int[], int[]> hist = LevelAnalyser.CalcHistogram(sum.data().asFloat(), edges);

        assertEquals(edges.length-1, hist.left.length);
        for(int i = 0; i < edges.length - 1; i++) {
            assertEquals(counts[i], hist.left[i]);
        }
        assertEquals(sum.length(), hist.right.length);
    }

    @Test
    public void TestPercentageLevel(){
        INDArray sum = Scores.sum(1);
        INDArray percentage = Nd4j.create(LevelAnalyser.PercentageLevel(sum.data().asFloat()));
        Assert.assertTrue(percentage.max().getFloat(0) <= 100.f);
        Assert.assertTrue(percentage.min().getFloat(0) >= 0.f);
    }

    @Test
    public void TestCombinedSelection(){
        ImmutablePair<int[], int[][]> I =
            LevelAnalyser.getCombinedSelection(scores);
        
        int[] IP = I.left; int[][] IC = I.right;

        assertEquals(46, IP.length);
        assertEquals(46, IC.length);

        assertEquals(3, IP[0]);
        assertEquals(1, IP[45]);

        assertEquals(2, IC[0][0]);
        assertEquals(7, IC[45][5]);
    }
}
