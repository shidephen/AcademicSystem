package com.thinkgem.jeesite.test;

import com.thinkgem.jeesite.modules.ans.tasks.*;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import oracle.jdbc.driver.OracleDriver;

/**
 * TestTasks
 */
public class TestTasks {
    public static final String URL = "jdbc:oracle:thin:@47.94.254.84:1521:orcl";
    public static final String USER = "airwebo";
    public static final String PASSWORD = "airwebo";
    public Connection connection;

    public TestTasks() throws SQLException, ClassNotFoundException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
    }

    /*
     *                   Steadiness Tests
    */
    @Test
    public void TestStandardDevTask() throws InterruptedException, ExecutionException{
        SteadinessAnaTask task = new SteadinessAnaTask(connection);
        float[][] cf = task.ApplyStandardDeviation(62);
        assertNotNull(cf);
    }

    @Test
    public void TestSubjectSteadiness() throws InterruptedException, ExecutionException{
        SteadinessAnaTask task = new SteadinessAnaTask(connection);
        int examId = 63;
        
        CompletableFuture<float[][]> cf = CompletableFuture.supplyAsync(
            () -> task.ApplyStandardDeviation(examId));
        float[][] std = cf.join();
        CompletableFuture<float[][]> SS = CompletableFuture.supplyAsync(
            () -> task.ApplySubjectSteadiness2(examId, std));
        float[][] R = SS.join();
        assertNotNull(R);
    }

    @Test
    public void TestCombinedSteadiness() throws InterruptedException, ExecutionException{
        int examId = 63;
        SteadinessAnaTask task = new SteadinessAnaTask(connection);
        CompletableFuture<float[][]> cf = CompletableFuture.supplyAsync(
            () -> task.ApplyStandardDeviation(examId))
            .thenApply(s -> task.ApplySubjectSteadiness(examId, s))
            .thenApply(ss -> task.ApplyCombinedSteadiness(examId, ss));
        
        float[][] R = cf.join();
        assertNotNull(R);
    }

    /*
     *              Level Tests
    */

    @Test
    public void TestNormalizedTask() throws InterruptedException, ExecutionException{
        LevelAnalysingTask ltask = new LevelAnalysingTask(connection);
        CompletableFuture<?> cf =  ltask.ApplyNormalizeScoreOfGrade(1);
        
        ImmutablePair<int[], float[][]> R = (ImmutablePair<int[], float[][]>)cf.get();
        assertNotNull(R);
        assertNotNull(R.left);
        assertNotNull(R.right);
    }

    @Test
    public void TestRelativeLevelTask() throws InterruptedException, ExecutionException{
        LevelAnalysingTask ltask = new LevelAnalysingTask(connection);
        int[] students = ltask.getDbUtils().getStudentIdsByExam(1);
        CompletableFuture[] tasks = new CompletableFuture[students.length];

        for(int i = 0; i < students.length; i++){
            int s = students[i];
            CompletableFuture f = 
                ltask.ApplyRelativeLevel(s);
            tasks[i] = f;
        }

        CompletableFuture ltasks = CompletableFuture.allOf(tasks);
        ltasks.join();
    }

    @Test
    public void TestCombinationLevelTask() throws InterruptedException, ExecutionException{
        LevelAnalysingTask ltask = new LevelAnalysingTask(connection);
        int[] students = ltask.getDbUtils().getStudentIdsByExam(1);
        CompletableFuture[] tasks = new CompletableFuture[students.length];

        for(int i = 0; i < students.length; i++){
            int s = students[i];
            CompletableFuture f = 
                ltask.ApplyCombinedLevel(s);
            tasks[i] = f;
        }

        CompletableFuture ltasks = CompletableFuture.allOf(tasks);
        ltasks.join();
    }

    @Test
    public void TestDifficultyTask() throws InterruptedException, ExecutionException{
        AssignmentAnaTask atask = new AssignmentAnaTask(connection);
        ImmutablePair<int[], float[]> R =  atask.ApplyDifficulty(1).get();
        assertNotNull(R);
        assertNotNull(R.left);
        assertNotNull(R.right);
    }
    
    @Test
    public void TestCombinationSelection() throws InterruptedException, ExecutionException{
        LevelAnalysingTask ltask = new LevelAnalysingTask(connection);
        ImmutablePair<int[], int[][]> S = ltask.ApplyCombinedSelection(462);
        assertNotNull(S);
        assertNotNull(S.left);
        assertNotNull(S.right);
    }
    @Test
    public void TestAllInTask(){
        AllInTask task = new AllInTask(connection);
        task.ApplyAll(5).join();
    }
}