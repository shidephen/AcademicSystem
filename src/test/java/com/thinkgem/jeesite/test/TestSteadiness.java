package com.thinkgem.jeesite.test;
import com.thinkgem.jeesite.modules.ans.AnalysingDefinations;
import org.junit.Test;
import static org.junit.Assert.*;
import com.thinkgem.jeesite.modules.ans.SteadinessAnalyser;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.ArrayList;

public class TestSteadiness {
    private float[][] scores1 = new float[][]{
        {38,53,52,48,50,62},
        {67,59,35,60,57,74},
        {90,78,64,58,66,62},
        {49,98,50,47.5f,70,56}
    };
    private float[] target_std = new float[]
        {22.730f, 20.347f, 11.899f, 6.549f, 8.995f, 7.550f};
    
    private float[][] stds = new float[][]{
        {22.730f, 20.347f, 11.899f, 6.549f, 8.995f, 7.550f},
        {12.447f, 18.998f, 8.461f, 4.444f, 6.047f, 5.802f},
        {4.655f, 20.106f, 9.416f, 5.477f, 8.693f, 4.082f},
        {22.664f, 15.327f, 12.553f, 8.855f, 7.867f, 6.850f},
        {21.747f, 24.221f, 10.935f, 6.562f, 3.109f, 7.974f}
    };

    private float[][] target_substeady = new float[][]{
        // 1 2 3 4 5
        // 5 4 3 2 1
        {5, 4, 4, 3, 5, 4},
        {2, 2, 1, 1, 2, 2},
        {1, 3, 2, 2, 4, 1},
        {4, 1, 5, 5, 3, 3},
        {3, 5, 3, 4, 1, 5}
    };

    int[][] combinations = new int[][]{
            {1, 3, 5},
            {2, 4, 6}
    };

    @Test
    public void TestStandardDeviation(){
        float[] std = SteadinessAnalyser.getScoresStd(scores1);
        for(int i = 0; i < std.length; i++)
            assertEquals(target_std[i], std[i], 1e-2);
    }

    @Test
    public void TestSubjectSteadiness(){
        float[][] steady = SteadinessAnalyser.getSubjectSteadinessP(stds);
        assertNotNull(steady);
        for(int i = 0; i < steady.length; i++){
            for(int j = 0; j < steady[0].length; j++)
                assertEquals(target_substeady[i][j], steady[i][j], 1e-4);
        }
    }

    @Test
    public void TestSubjectSteadiness2(){
        float[][] steady = SteadinessAnalyser.getSubjectSteadiness2(stds);
        assertNotNull(steady);
        assertEquals(5, steady.length);
        assertEquals(6, steady[0].length);
        assertArrayEquals(new float[]{6,5,4,1,3,2}, steady[0], 0.1f);
    }

    @Test
    public void TestCombinationSteadiness(){
        float[][] steady = SteadinessAnalyser.getCombinedSteadinessP(target_substeady, combinations);
        assertNotNull(steady);
    }
}
